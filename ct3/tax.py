import ast
import pandas as pd
from ctt_tax import *
from collections import defaultdict
import multiprocessing as mp
from multiprocessing import Process, Pipe
from statistics import mean
import cosy
from sklearn.cluster import KMeans
from tax_encoder import *
from tqdm import tqdm
from vars_collector import *


ast_node_types = dir(ast)[:107]+["defaults",
                                 "decorator_list",
                                 "ListLoad",
                                 "TupleLoad"]


def categorize(ast_file_path, categories, num_procs=1):
    """Multiprocessed code token type generation for AST nodes.

    Args:
        ast_file_path (Path): The name of the file that contains ASTs
        categories(list): List with enabled categories
        num_procs (int): Number of processes (default = 1)

    Return
        vec_result (list(list(list))): numerical code token type vectors

        token_result (list(string)): collected code tokens

        ast_ids_result (list(int)): ID of the AST in the AST file starting from 0
        node_ids_result (list(int)): ID of the node in the AST starting from 0
    """
    print("num cpus in system: ", mp.cpu_count())

    vec_to_tax = defaultdict(list)
    frequencies = defaultdict(int)
    vec_to_tokens = defaultdict(lambda: defaultdict(int))
    processes = list()

    with cosy.data.Python150kDataset(ast_file_path,
                                     expand_values=True) as dataset:
        ast_list = [a for a in dataset]
        size_dataset = len(ast_list)
        data_per_proc = size_dataset / num_procs

        vec_per_proc = list()
        token_per_proc = list()
        node_ids_per_proc = list()
        vec_result = list()
        token_result = list()
        ast_ids_result = list()
        node_ids_result = list()
        conn = [(Pipe()) for i in range(num_procs)]

        for i in range(num_procs):
            start = int(i * data_per_proc)
            offset = int(start + data_per_proc)
            proc = Process(target=tax_it,
                           args=(ast_list[start:offset],
                                 categories,
                                 conn[i][1]))
            proc.start()
            processes.append(proc)

        for i in tqdm(range(num_procs)):
            vec_per_proc.append(conn[i][0].recv())
            token_per_proc.append(conn[i][0].recv())
            node_ids_per_proc.append(conn[i][0].recv())

        for p in processes:
            p.join()

        for vec_list, token_list in zip(vec_per_proc, token_per_proc):
            for vec, token in zip(vec_list, token_list):
                vec_result.append(vec)
                token_result.append(token)

        for p_id, node_ids in enumerate(node_ids_per_proc):
            for ast_id, n_ids in enumerate(node_ids):
                ast_ids_result += [int(data_per_proc*p_id+ast_id)]*len(n_ids)
                node_ids_result += n_ids

    return vec_result, token_result, ast_ids_result, node_ids_result


def tax_it(ast_list, categories, conn):
    """Target subroutine for each process called in categorized. Operates on
       the assigned data split from categeorize and generates the code token
       types.

    Args:
        ast_file_list (list): Data split of ast file
        categories(list): List with enabled categories
        conn (Pipe()): Pipe to send back the results to the root process
    """
    ctt_tax = CttTax(categories)
    local_vec_to_tokens = defaultdict(list)
    vec = list()
    token = list()
    node_ids = [list() for _ in range(len(ast_list))]

    ast_id = 0
    for a in tqdm(ast_list):
        ctt_tax.update_categories(a)
        collector = VarsCollector(cosy.tree.dfs(a))
        collector.collect_vars()
        for n_id, v in enumerate(cosy.tree.dfs(a)):
            if is_leaf(v) and v["symbol"] not in ast_node_types:
                if len("".join(v["symbol"].split())) > 30:
                    vec.append([[-1]*ctt.max_features]*len(ctt_tax.categories))
                    token.append("<UNKNOWN>")
                else:
                    vec.append(ctt_tax.get_taxonomy(v, collector.vars_def))
                    token.append("".join(v["symbol"].split()))
            else:
                vec.append([[-1]*ctt.max_features]*len(ctt_tax.categories))
                if len("".join(v["symbol"].split())) > 30:
                    token.append("<UNKNOWN>")
                else:
                    token.append("".join(v["symbol"].split()))
            node_ids[ast_id].append(n_id)
        ast_id += 1

    conn.send(vec)
    conn.send(token)
    conn.send(node_ids)
    conn.close()


def clustering(ctt, vecs):
    """Clusters code to token types with K-means.

    Args:
        vecs (list): numerical code token type vectors with padding

    Return
        list(list) (list): List containing lists with pairs of groups and
                           flattened feature vectors without padding
    """
    new_vecs = list()
    for vec in vecs:
        new_vecs.append(ctt.remove_padding(ctt.flatten(vec)))

    kmeans = KMeans(n_clusters=21)
    kmeans.fit(new_vecs)

    return [[group, vec] for group, vec in zip(kmeans.labels_, new_vecs)]


def create_tax(tax_csv, token_csv, syntax_csv, context_csv, origin_csv,
               length_csv, frequency_csv, astidx_csv, nodeidx_csv, chunksize):
    print("Reading from small csvs and write to a large one...")
    token = pd.read_csv(token_csv, header=0,
                        index_col=False, chunksize=chunksize)
    syntax = pd.read_csv(syntax_csv, header=0,
                         index_col=False, chunksize=chunksize)
    context = pd.read_csv(context_csv, header=0,
                          index_col=False, chunksize=chunksize)
    origin = pd.read_csv(origin_csv, header=0,
                         index_col=False, chunksize=chunksize)
    length = pd.read_csv(length_csv, header=0,
                         index_col=False, chunksize=chunksize)
    frequency = pd.read_csv(frequency_csv, header=0,
                            index_col=False, chunksize=chunksize)
    astidx = pd.read_csv(astidx_csv, header=0,
                         index_col=False, chunksize=chunksize)
    nodeidx = pd.read_csv(nodeidx_csv, header=0,
                          index_col=False, chunksize=chunksize)

    header = True
    for token_ck, syntax_ck, context_ck, origin_ck, length_ck, frequency_ck, astidx_ck, nodeidx_ck\
            in zip(token, syntax, context, origin, length, frequency, astidx, nodeidx):
        data = [token_ck.iloc[:,1:], syntax_ck.iloc[:,1:], context_ck.iloc[:,1:], origin_ck.iloc[:,1:],
                length_ck.iloc[:,1:], frequency_ck.iloc[:,1:], astidx_ck.iloc[:,1:], nodeidx_ck.iloc[:,1:]]
        result = pd.concat(data, axis=1)
        result.to_csv(tax_csv, header=header, mode="a")
        header = False


if __name__ == '__main__':
    NUM_PROCS = 16
    CHUNKSIZE = 10**6
    # data_path = "tests/data/a_simple_ast.json"
    # data_path = "tests/data/an_ast.json"
    data_path = "/export/home/tuyenle/repos/data/python100k_train.json"
    # data_path = "/export/home/tuyenle/repos/data/python50k_eval.json"

    tax_csv = "/export/home/tuyenle/repos/data/csv/tax_train_100k.csv"
    token_csv = "/export/home/tuyenle/repos/data/csv/token_train_100k.csv"
    syntax_csv = "/export/home/tuyenle/repos/data/csv/syntax_train_100k.csv"
    context_csv = "/export/home/tuyenle/repos/data/csv/context_train_100k.csv"
    origin_csv = "/export/home/tuyenle/repos/data/csv/origin_train_100k.csv"
    length_csv = "/export/home/tuyenle/repos/data/csv/length_train_100k.csv"
    frequency_csv = "/export/home/tuyenle/repos/data/csv/frequency_train_100k.csv"
    astidx_csv = "/export/home/tuyenle/repos/data/csv/astidx_train_100k.csv"
    nodeidx_csv = "/export/home/tuyenle/repos/data/csv/nodeidx_train_100k.csv"

    # tax_csv = "/export/home/tuyenle/repos/data/csv/tax_eval_50k.csv"
    # token_csv = "/export/home/tuyenle/repos/data/csv/token_eval_50k.csv"
    # syntax_csv = "/export/home/tuyenle/repos/data/csv/syntax_eval_50k.csv"
    # context_csv = "/export/home/tuyenle/repos/data/csv/context_eval_50k.csv"
    # origin_csv = "/export/home/tuyenle/repos/data/csv/origin_eval_50k.csv"
    # length_csv = "/export/home/tuyenle/repos/data/csv/length_eval_50k.csv"
    # frequency_csv = "/export/home/tuyenle/repos/data/csv/frequency_eval_50k.csv"
    # astidx_csv = "/export/home/tuyenle/repos/data/csv/astidx_eval_50k.csv"
    # nodeidx_csv = "/export/home/tuyenle/repos/data/csv/nodeidx_eval_50k.csv"

    tax_encoder_data_path = None

    categories = ["SyntaxType", "Context", "Origin", "Length", "Frequency"]
    ctt = CttTax(categories)

    vec_result, token_result, ast_ids_result, node_ids_result = categorize(data_path,
                                                                           categories,
                                                                           num_procs=NUM_PROCS)

    # if tax_encoder_data_path:
    #     encoder_tax = TaxEncoder.load(tax_encoder_data_path)
    # else:
    #     groups_and_vecs = clustering(ctt, vec_result)
    #     encoder_tax = TaxEncoder(groups_and_vecs)

    # groups = list()

    syntax_type = list()
    context = list()
    origin = list()
    length = list()
    frequency = list()

    # compress feature vector information into a single number
    # from 1 - max number of features in category
    for vec in vec_result:
        # groups.append(int(encoder_tax.encode(str(vec))))

        # if token has syntax_type
        if vec[0][0] != -1:
            syntax_type.append(vec[0].index(1)+1)
            temp = [(i+1, feat) for i, feat in enumerate(vec[1]) if feat != 0]
            context.append(temp if temp else [0])
            origin.append(vec[2].index(1)+1)
            length.append(vec[3].index(1)+1)
            frequency.append(vec[4].index(1)+1)
        else:
            syntax_type.append(-1)
            context.append(-1)
            origin.append(-1)
            length.append(-1)
            frequency.append(-1)

    # write all the lists to csv for saving
    print("Creating 8 dfs for 8 lists...")
    token_df = pd.DataFrame(token_result, columns=["token"])
    syntax_df = pd.DataFrame(syntax_type, columns=["syntax_type"])
    context_df = pd.DataFrame(context, columns=["context"])
    origin_df = pd.DataFrame(origin, columns=["origin"])
    length_df = pd.DataFrame(length, columns=["length"])
    frequency_df = pd.DataFrame(frequency, columns=["frequency"])
    astidx_df = pd.DataFrame(ast_ids_result, columns=["AST_idx"])
    nodeidx_df = pd.DataFrame(node_ids_result, columns=["node_idx"])

    print("Created 8 dfs successfully. Writing them to csv files...")
    token_df.to_csv(path_or_buf=token_csv, mode="w",
                    sep=",", chunksize=CHUNKSIZE)
    syntax_df.to_csv(path_or_buf=syntax_csv, mode="w",
                     sep=",", chunksize=CHUNKSIZE)
    context_df.to_csv(path_or_buf=context_csv, mode="w",
                      sep=",", chunksize=CHUNKSIZE)
    origin_df.to_csv(path_or_buf=origin_csv, mode="w",
                     sep=",", chunksize=CHUNKSIZE)
    length_df.to_csv(path_or_buf=length_csv, mode="w",
                     sep=",", chunksize=CHUNKSIZE)
    frequency_df.to_csv(path_or_buf=frequency_csv, mode="w",
                        sep=",", chunksize=CHUNKSIZE)
    astidx_df.to_csv(path_or_buf=astidx_csv, mode="w",
                     sep=",", chunksize=CHUNKSIZE)
    nodeidx_df.to_csv(path_or_buf=nodeidx_csv, mode="w",
                      sep=",", chunksize=CHUNKSIZE)

    print("8 csv files are wrote successfully.")

    create_tax(tax_csv, token_csv, syntax_csv, context_csv, origin_csv,
               length_csv, frequency_csv, astidx_csv, nodeidx_csv, CHUNKSIZE)
    
    print("Done!")
