import json
from collections import defaultdict
from pathlib import Path
import cosy
from tqdm import tqdm
from ctt_tax import *
import itertools
from multiprocessing import Process, Manager, Pipe
import ast
from sklearn.cluster import KMeans
from tax_encoder import *


ast_node_types = dir(ast)[:107] + ["defaults",
                                   "decorator_list",
                                   "ListLoad",
                                   "TupleLoad"]


def build_encoder(in_file, vocab_size):
    """Build the encoder that encodes our symbols as integers.

    Args:
        in_file (Path): The file that contains the raw datapoints.
        vocab_size (int): The size of our encoder vocabulary.
        We only encode the (vocab_size - 2) most frequent symbols
        together with the padding and OOV symbol.

    Returns:
        (StaticSymbolEncoder) The encoder.
    """
    frequencies = defaultdict(int)

    # Dataset format: [{"type": <str>, "value": <str>,  "children": list[int]}, ...]
    # Note: Some datapoints do not have all 3 attributes.
    print("Building code encoder...")
    with cosy.data.Dataset(in_file) as dataset:
        for line in tqdm(dataset):
            for node in json.loads(line):
                if "value" in node:
                    frequencies[node["value"]] += 1

                if "type" in node:
                    frequencies[node["type"]] += 1

    # sorted_frequencies is a list of (symbol, frequency) pairs.
    sorted_frequencies = sorted(frequencies.items(),
                                key=lambda x: x[1],
                                reverse=True)

    # The encoder will always add the two special symbols <PADDING> and <UNKNOWN>.
    sorted_frequencies = sorted_frequencies[:(vocab_size - 2)]

    return cosy.preprocessing.StaticSymbolEncoder((symbol for
                                                   symbol, _
                                                   in sorted_frequencies))


def build_encoder_taxonomy(in_file, categories, vocab_size, num_procs=1):
    """Build the encoder that encodes our taxonomy as integers.

    Args:
        in_file (Path): The file that contains the raw datapoints.
        categories (list): List of enabled categories
        vocab_size (int): The size of our encoder vocabulary.
                          We only encode the (vocab_size - 2) most
                          frequent symbols together with the padding
                          and OOV symbol.
        num_procs (int): number of worker processes

    Returns:
        TaxEncoder): The encoder for taxonomy.
    """
    frequencies = defaultdict(int)
    num_procs = num_procs
    processes = list()
    freq_per_proc = list()
    conn = [(Pipe()) for i in range(num_procs)]

    print("Building taxonomy encoder...")
    with cosy.data.Python150kDataset(in_file, expand_values=True) as dataset:
        ast_list = [ast for ast in dataset]
        size_dataset = len(ast_list)
        data_per_proc = size_dataset / num_procs

        for i in range(num_procs):
            start = int(i * data_per_proc)
            offset = int(start + data_per_proc)
            proc = Process(target=tax_it,
                           args=(ast_list[start:offset],
                                 categories,
                                 conn[i][1]))
            proc.start()
            processes.append(proc)

        for i in tqdm(range(num_procs)):
            freq_per_proc.append(conn[i][0].recv())

        for p in tqdm(processes):
            p.join()

        for key, val in itertools.chain(*[freq.items()
                                          for freq in freq_per_proc]):
            frequencies[key] += val

    sorted_frequencies = sorted(frequencies.items(),
                                key=lambda x: x[1],
                                reverse=True)
    if len(sorted_frequencies) >= (vocab_size):
        sorted_frequencies = sorted_frequencies[:(vocab_size)]

    groups_and_vecs = clustering(sorted_frequencies, categories)

    return TaxEncoder(groups_and_vecs)


def tax_it(ast_list, categories, conn):
    """Target subroutine for each process called in build_encoder_taxonomy.
       Operates on the assigned data split from build_encoder_taxonomy
       and generates the code token types.

    Args:
        ast_list (list): Data split of ast file
        categories(list): List with enabled categories
        conn (Pipe()): Pipe to send back the results to the root process
    """
    ctt = CttTax(categories)
    local_frequencies = defaultdict(int)
    for tree in tqdm(ast_list):
        ctt.update_categories(tree)
        for v in cosy.tree.dfs(tree):
            if is_leaf(v) and v["symbol"] not in ast_node_types:
                local_frequencies[str(ctt.get_taxonomy(v))] += 1
    conn.send(local_frequencies)
    conn.close()


def clustering(sorted_frequencies, categories):
    """Clusters code to token types with K-means.

    Args:

        categories (list): List of enabled categories
        sorted_frequencies list(tuple): list with tuples containing numerical
                                        code token type vector and frequency,
                                        sorted by frequency in descending
                                        order.

    Return
        df (dataframe): Columns: group, numerical code token type vector,
                        literal code token type
    """
    tqdm.pandas()
    ctt = CttTax(categories)

    new_vecs = list()
    for v in sorted_frequencies:
        new_vecs.append(ctt
                        .remove_padding(ctt.
                                        flatten(ast.literal_eval(v[0]))))

    kmeans = KMeans(n_clusters=20)
    kmeans.fit(new_vecs)

    old_vecs = list()
    for v in new_vecs:
        old_vecs.append(ctt.to_two_d(ctt.add_padding(v)))
    return [(group, old_v) for group, old_v in zip(kmeans.labels_, old_vecs)]


def gather_data(ast_list,
                encoder_code,
                encoder_tax,
                vertices_code,
                vertices_tax,
                categories):
    """Target subroutine for each process called in run.
       Operates on the assigned data split from run and encodes
       code tokens and their code token types.

    Args:
        ast_list (list): Data split of ast file
        encoder_code (StaticSymbolEncoder): The encoder for code tokens.
        encoder_tax (TaxEncoder): The encoder for taxonomy.
        vertices_code (list): List that stores code token encodings
        vertices_tax (list): List that stores code token type encodings
        categories(list): List with enabled categories
    """
    ctt_tax = CttTax(categories)
    for tree in tqdm(ast_list):
        # Get all vertices in DFS order and encode their respective symbols.
        vertices_code.append([encoder_code.encode(v["symbol"])
                              for v in cosy.tree.dfs(tree)])

        ctt_tax.update_categories(tree)
        vertices_tax.append([int(encoder_tax.encode(str(ctt_tax
                                                        .get_taxonomy(v))))
                             for v in cosy.tree.dfs(tree)])


def run(in_file,
        out_file,
        encoder_code,
        window_size,
        step_size,
        compression,
        encoder_tax,
        categories,
        num_procs=1):
    """Rune the preprocessing pipeline and create the tfrecord files.

    Args:
        in_file (Path): The name of the file that contains the raw datapoints.
        out_file (Path): The file where the tfrecords will be written to.
        encoder_code (StaticSymbolEncoder): The encoder for code tokens.
        window_size (int): Size of the sliding window.
        step_size (int): Step size of the sliding window.
        compression (str): The type of compression that will be applied
                           to the output file.
                           Must be either 'GZIP', 'ZLIP' or None.
        encoder_tax (TaxEncoder): The encoder for taxonomy.
        categories (list): List of enabled categories
        num_procs (int): number of worker processes
    """
    writer_config = {"filename": str(out_file), "compression": compression}
    vertices_code = list()
    vertices_tax = list()
    num_procs = num_procs
    processes = list()

    # We map each AST in the Python150kDataset to a sequence of vertices is DFS order.
    with Manager() as manager:
        with cosy.data.Python150kDataset(in_file,
                                         expand_values=True) as dataset:
            ast_list = [ast for ast in dataset]
            size_dataset = len(ast_list)
            proc_vertices_code = [manager.list() for i in range(num_procs)]
            proc_vertices_tax = [manager.list() for i in range(num_procs)]
            data_per_proc = size_dataset / num_procs
            for i in range(num_procs):
                start = int(i * data_per_proc)
                offset = int(start + data_per_proc)
                proc = Process(target=gather_data,
                               args=(ast_list[start:offset],
                                     encoder_code,
                                     encoder_tax,
                                     proc_vertices_code[i],
                                     proc_vertices_tax[i],
                                     categories))
                proc.start()
                processes.append(proc)

            for p in tqdm(processes):
                p.join()

            n_tax = (encoder_tax._vocab_size - 1)
            with cosy.data.SequenceWriter(["int32", "bool"] + ["bool"] * n_tax,
                                          2 + (encoder_tax._vocab_size - 1),
                                          **writer_config) as writer:
                for vertices_code, vertices_tax in zip(proc_vertices_code,
                                                       proc_vertices_tax):
                    for v_c, v_t in zip(vertices_code, vertices_tax):
                        for (window_code, mask_code),\
                            (window_tax, _)\
                            in zip(cosy
                                   .preprocessing
                                   .window_iter(v_c, window_size, step_size),
                                   cosy
                                   .preprocessing
                                   .window_iter(v_t, window_size, step_size)):

                            tax_masks = list()
                            for i in range(encoder_tax._vocab_size - 1):
                                tax_masks.append([tax == i for tax
                                                  in window_tax])

                            writer(sequences=[window_code,
                                              mask_code] + tax_masks)


if __name__ == "__main__":
    VOCAB_SIZE = 10000
    WINDOW_SIZE = 1000
    STEP_SIZE = 500
    NUM_PROCS_TRAIN = 32
    NUM_PROCS_EVAL = 16
    COMPRESSION = "GZIP"
    FOLDER = Path("data")

    train_in_file = FOLDER / "python100k_train.json"
    eval_in_file = FOLDER / "python50k_eval.json"
    train_out_file = FOLDER / "dfs_train.tfrecord"
    eval_out_file = FOLDER / "dfs_eval.tfrecord"
    encoding_file_code = FOLDER / "encoder_code.json"
    encoding_file_tax = FOLDER / "encoder_tax.json"
    vocab_size_file = FOLDER / "vocab_size.txt"

    # Create the root folder if necessary.
    FOLDER.mkdir(parents=True, exist_ok=True)

    # Download the data if it does not already exist.
    # if not (train_in_file.is_file() and eval_in_file.is_file()):
    #     cosy.data.Python150kDataset.download(path=FOLDER)

    # ctt_tax = CttTax(["SyntaxType", "Context", "Origin", "Length"])
    categories = ["SyntaxType", "Context", "Origin", "Length", "Frequency"]

    # Create the encoding if it does not already exist.
    if not encoding_file_code.is_file():
        encoder_code = build_encoder(train_in_file, VOCAB_SIZE)
        encoder_code.save(encoding_file_code)
    else:
        encoder_code = cosy.preprocessing\
            .StaticSymbolEncoder.load(encoding_file_code)

    if not encoding_file_tax.is_file():
        encoder_tax = build_encoder_taxonomy(train_in_file, categories,
                                             VOCAB_SIZE,
                                             num_procs=NUM_PROCS_TRAIN)
        encoder_tax.save(encoding_file_tax)
    else:
        encoder_tax = TaxEncoder.load(encoding_file_tax)

    # Create the trainings data.
    run(train_in_file, train_out_file, encoder_code, WINDOW_SIZE, STEP_SIZE,
        COMPRESSION, encoder_tax, categories, num_procs=NUM_PROCS_TRAIN)

    # Create the evaluation data.
    run(eval_in_file, eval_out_file, encoder_code, WINDOW_SIZE, STEP_SIZE,
        COMPRESSION, encoder_tax, categories, num_procs=NUM_PROCS_EVAL)

    with open(vocab_size_file, "w") as f:
        json.dump([encoder_code._vocab_size, encoder_tax._vocab_size], f)
