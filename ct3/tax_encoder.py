# Copyright (c) 2021 Gabriel Rashidi <g.rashidi@stud.uni-heidelberg.de>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from collections import defaultdict
import json


class TaxEncoder:
    def __init__(self,
                 groups_and_vecs,
                 oov_symbol="<UNKNOWN>",
                 return_list=False):
        self._str2int = {}
        self._int2str = defaultdict(list)

        for group, tax in groups_and_vecs:
            self._str2int[str(tax)] = str(group)
            self._int2str[str(group)].append(str(tax))

        self._vocab_size = len(self._int2str) + 1
        self._oov_symbol = oov_symbol
        self._return_list = return_list

    @property
    def encoding(self):
        """The map used to encode the symbols."""
        return self._str2int

    @property
    def decoding(self):
        """The map used to decode the symbols."""
        return self._int2str

    @property
    def vocab_size(self):
        """The size of the vocabulary including oov symbol."""
        return self._vocab_size

    @property
    def oov_symbol(self):
        """The symbol used for encodings which are oov."""
        return self._oov_symbol

    @property
    def return_list(self):
        """If True, the `encode` method will insert the codes into a list before returning them."""
        return self._return_list

    @return_list.setter
    def return_list(self, value):
        self._return_list = value

    def encode(self, symbol):
        """Encode a symbol.

        Args:
            symbol (str): The symbol to be encoded.

        Returns:
            (int) The code for this symbol.
        """
        try:
            code = self._str2int[symbol]
        except KeyError:
            code = self.vocab_size - 1  # oov code

        if self._return_list:
            return [code]

        return code

    def decode(self, number):
        """Decode a number.

        Args:
            number (int): The number to be decoded.

        Returns:
            (str) The decoded string.

        Raises:
            AssertionError: If `number` is negative.
        """
        assert number >= 0, "Decodes symbols must be positive integers."

        try:
            return self._int2str[number]
        except IndexError:
            return self._oov_symbol

    def has_encoding(self, symbol):
        """Check if an encoding for the given symbol exists.

        Args:
            symbol (str): The symbol to be checked.

        Returns:
            (bool) True if the symbol has an encoding, false otherwise.
        """
        if symbol == self.oov_symbol:
            return False

        return symbol in self._str2int

    def is_encoding(self, number):
        """Check if number encodes a symbol.

        Args:
            number (int): The number to be checked.

        Returns:
            (bool) True if the number is an encoding, false otherwise.
        """
        return 0 <= number < self.vocab_size - 1

    def get_config(self):
        """The the configuration of this symbol encoder instance.

        Returns:
            (dict): The symbol to code and code to symbol mappings.
        """
        return {
            "encoding": self._str2int,
            "decoding": self._int2str,
            "oov_symbol": self._oov_symbol,
        }

    def save(self, filename):
        """Save the encoding to disk.

        Args:
            filename (str): The file where the encoding will be saved to.
        """
        with open(filename, "w") as f:
            json.dump(self.get_config(), f, indent=4)

    @classmethod
    def load(cls, filename):
        """Load the encoding from disk.

        Args:
            filename (str): The file that contains the saved encoding.
        """
        with open(filename, "r") as f:
            data = json.load(f)

            encoder = TaxEncoder([])

            encoder._str2int = data["encoding"]
            encoder._int2str = data["decoding"]
            encoder._oov_symbol = data["oov_symbol"]
            encoder._vocab_size = len(encoder._int2str) + 1

        return encoder
