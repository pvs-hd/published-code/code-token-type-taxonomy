# Copyright (c) 2021 Gabriel Rashidi <g.rashidi@stud.uni-heidelberg.de>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import matplotlib.pyplot as plt
import ast
import pandas as pd
from ctt_tax import *
from collections import defaultdict, Counter
import itertools
import multiprocessing as mp
from multiprocessing import Process, Pipe
from statistics import mean, StatisticsError
from scipy.stats import pearsonr
import numpy as np
import cosy
from sklearn.preprocessing import StandardScaler
from pca import pca
from sklearn.cluster import KMeans
import json
from tax_encoder import *
import os
from tqdm import tqdm


ast_node_types = dir(ast)[:107] + ["defaults",
                                   "decorator_list",
                                   "ListLoad",
                                   "TupleLoad"]


def categorize(ast_file_path, categories, num_procs=1):
    """Multiprocessed code token type generation for AST nodes.

    Args:
        ast_file_path (Path): The name of the file that contains ASTs
        categories(list): List with enabled categories
        num_procs (int): Number of processes (default = 1)

    Return
        vec_to_tax (defaultdict(list)): numerical code token type vector mapped
                                        to literal code token types
        frequencies (defaultdict(int)): literal code token types mapped to
                                        their frequency
        tax_to_tokens (defaultdict(defaultdict(int))): literal code token types
                                                       mapped to their related
                                                       code tokens
    """
    print("num cpus in system: ", mp.cpu_count())

    vec_to_tax = defaultdict(list)
    frequencies = defaultdict(int)
    tax_to_tokens = defaultdict(lambda: defaultdict(int))
    processes = list()

    with cosy.data.Python150kDataset(ast_file_path,
                                     expand_values=True) as dataset:
        ast_list = [a for a in dataset]
        size_dataset = len(ast_list)
        data_per_proc = size_dataset / num_procs
        vec_to_tax_per_proc = list()
        freq_per_proc = list()
        tax_to_tokens_per_proc = list()
        conn = [(Pipe()) for i in range(num_procs)]

        for i in range(num_procs):
            start = int(i * data_per_proc)
            offset = int(start + data_per_proc)
            proc = Process(target=tax_it,
                           args=(ast_list[start:offset],
                                 categories,
                                 conn[i][1]))
            proc.start()
            processes.append(proc)

        for i in tqdm(range(num_procs)):
            vec_to_tax_per_proc.append(conn[i][0].recv())
            freq_per_proc.append(conn[i][0].recv())
            tax_to_tokens_per_proc.append(conn[i][0].recv())

        for p in processes:
            p.join()

        for key, val in itertools.chain(*[d.items()
                                        for d in vec_to_tax_per_proc]):
            vec_to_tax[key] += val

        for key, val in itertools.chain(*[freq.items()
                                        for freq in freq_per_proc]):
            frequencies[key] += val

        temp = defaultdict(list)
        for key, vals in itertools.chain(*[tax_to_tokens.items()
                                         for tax_to_tokens
                                         in tax_to_tokens_per_proc]):
            temp[key] += vals
        for key, vals in temp.items():
            tax_to_tokens[key] = Counter(vals)

    return vec_to_tax,\
        sorted(frequencies.items(), key=lambda x: x[1], reverse=True),\
        tax_to_tokens


def tax_it(ast_list, categories, conn):
    """Target subroutine for each process called in categorized. Operates on
       the assigned data split from categeorize and generates the code token
       types.

    Args:
        ast_file_list (list): Data split of ast file
        categories(list): List with enabled categories
        conn (Pipe()): Pipe to send back the results to the root process
    """
    ctt_tax = CttTax(categories)
    local_vec_to_tax = defaultdict(list)
    local_frequencies = defaultdict(int)
    local_tax_to_tokens = defaultdict(list)
    for a in tqdm(ast_list):
        ctt_tax.update_categories(a)
        for v in cosy.tree.dfs(a):
            if is_leaf(v) and v["symbol"] not in ast_node_types:
                vec = ctt_tax.get_taxonomy(v)
                tax = str(ctt_tax.translate_to_literal(vec))
                local_vec_to_tax[str(vec)] = [tax]
                local_frequencies[tax] += 1
                local_tax_to_tokens[tax] += [v["symbol"]]

    conn.send(local_vec_to_tax)
    conn.send(local_frequencies)
    conn.send(local_tax_to_tokens)
    conn.close()


def group_by_20_most_frequent_code_token_types(df):
    """Groupes the code token types by Jaccard similarity with the 20 most
       frequent code token types as reference.

    Args:
        df (dataframe): Contains columns literal code token types and
                        frequency

    Return
        buckets (defaultdict(list)): Group number mapped to list of tuples
                                     containing literal code token type and
                                     Jaccard similarity regarding reference
                                     code token type
    """
    buckets = defaultdict(list)
    most_freq_num = 20
    [buckets[i].append((tax, 1.))
     for i, tax in enumerate(df[:most_freq_num]["taxonomy"])]

    for tax_a, freq_a in zip(df[most_freq_num:]["taxonomy"],
                             df[most_freq_num:]["frequency"]):
        js = list()
        for tax_b in df[:most_freq_num]["taxonomy"]:
            js.append(jaccard_similarity(tax_a, tax_b))

        bucket_num = np.argmax(js)
        buckets[bucket_num] += [(tax_a, js[bucket_num])]
    return buckets


def unkown_syntax_type_analysis(tax_to_tokens):
    """Analyzes unknown code token types' unknown syntax types.

    Args:
        tax_to_tokens (defaultdict(defaultdict(int))): literal code token types
                                                       mapped to their related
                                                       code tokens

    Return
        total_num_code_tokens (int): total number of code tokens in dataset
        unknown_num_code_tokens (int): total number of code token types with
                                       an unknown syntax type
        unknown_code_tokens (defaultdict(int)): code tokens assigned to
                                                a code token type with an
                                                unknown syntax type mapped to
                                                their frequency
    """
    unknown_code_tokens = defaultdict(int)
    total_num_code_tokens = 0
    unknown_num_code_tokens = 0
    for tax, code_tokens in tax_to_tokens.items():
        total_num_code_tokens += sum(code_tokens.values())

        if ast.literal_eval(tax)[0] == ["znknown"]:
            unknown_num_code_tokens += sum([num
                                            for _, num in code_tokens.items()])
            for token, num in code_tokens.items():
                unknown_code_tokens[token] += num

    return total_num_code_tokens, unknown_num_code_tokens, unknown_code_tokens


def clustering(vec_to_tax, vecs, new_vecs):
    """Clusters code to token types with K-means.

    Args:
        vec_to_tax (defaultdict(list)): numerical code token type vector mapped
                                        to literal code token types
        vecs (list): numerical code token type vectors with padding
        new_vecs (list): numerical code token type vectors without padding
    Return
        df (dataframe): Columns: group, numerical code token type vector,
                        literal code token type
    """
    kmeans = KMeans(n_clusters=20)
    kmeans.fit(new_vecs)
    df = pd.DataFrame(list(zip(kmeans.labels_,
                               new_vecs,
                               [vec_to_tax[str(v)][0] for v in vecs])),
                      columns=["group", "vec", "tax"])

    return df


def get_clustering_from_encoder_file(tax_encoder_data_path,
                                     vec_to_tax,
                                     vecs,
                                     new_vecs):
    """Retrieves the cluster mapping from the encoder file.

    Args:
        ast_file_path (Path): The name of the file that contains taxonomy
                              encodings
        vec_to_tax (defaultdict(list)): numerical code token type vector mapped
                                        to literal code token types
        vecs (list): numerical code token type vectors with padding
        new_vecs (list): numerical code token type vectors without padding
    Return
        df (dataframe): Columns: group, numerical code token type vector,
                        literal code token type
    """
    literal_tax = [vec_to_tax[str(v)][0] for v in vecs]
    encoder_tax = TaxEncoder.load(tax_encoder_data_path)
    groups = [int(encoder_tax.encode(str(v))) for v in vecs]

    df = pd.DataFrame(list(zip(groups, new_vecs, literal_tax)),
                      columns=["group", "vec", "tax"])

    return df


def group_similarity_analysis(df, tax_freq):
    """Analyzes code token type groups regarding similarity and quantity of
       code token types and code tokens. Writes results to file.

    Args:
        df (dataframe): Columns: group, numerical code token type vector,
                        literal code token type
        tax_freq (defaultdict(int)): literal code token types mapped to
                                     their frequency
    Return
        df (dataframe): Columns: group, Jaccard similarity, Pearson correlation,
                        code token type count, code token count
    """
    tqdm.pandas()

    token_count = defaultdict(int)
    for tax, freq in tax_freq:
        token_count[df["group"][(df["tax"] == tax)].iloc[0]] += freq
    token_count = sorted(token_count.items(),
                         key=lambda x: x[0],
                         reverse=False)
    token_count = [x[1] for x in token_count]
    token_count = pd.DataFrame(token_count, columns=["token_count"])

    type_count = df[["group", "vec"]].groupby("group").count()
    type_count.columns = ["type_count"]

    if "js" in df.columns:
        df["pr"] = df.progress_apply(lambda x: pearson(x.group, x.vec, df),
                                     axis=1)
    else:
        df["js"] = df.progress_apply(lambda x: similarity(x.group, x.tax, df),
                                     axis=1)
        df["pr"] = df.progress_apply(lambda x: pearson(x.group, x.vec, df),
                                     axis=1)

    df = df[["group", "js", "pr"]].groupby("group").mean()
    df = df.join(type_count, lsuffix='_df', rsuffix='_type_count')
    df = df.join(token_count, lsuffix='_df', rsuffix='_token_count')

    df.to_csv(path_or_buf="ctt_strings.txt", mode="a", sep="\t")

    return df


def pca_analysis(ctt, df, new_vecs):
    """PCA of code token types. Writes dataframe with columns containing
       PC, top feature, cumulative explained variance to file. Plots biplot.


    Args:
        ctt (CTT()): CttTax object
        df (dataframe): Columns: group, numerical code token type vector,
                        literal code token type
        new_vecs (list): numerical code token type vectors without padding
    """
    x = StandardScaler().fit_transform(new_vecs)
    X = pd.DataFrame(data=x, columns=[f for feat in ctt.features
                                      for f in feat])
    model = pca()
    out = model.fit_transform(X)

    fig = plt.figure(figsize=(8, 8))
    model.biplot(n_feat=10, y=df["group"], legend=True)
    fig.tight_layout()
    plt.savefig("biplot2d.png")

    pd.concat([out["topfeat"]["PC"],
              out["topfeat"]["feature"],
              pd.Series(out["explained_var"])],
              axis=1, ignore_index=True).loc[:41]\
      .to_csv(path_or_buf="ctt_strings.txt", mode="a", sep="\t")


def analyze_group_feature_composition_most_frequent(ctt, buckets):
    """Analyzes code token type groups' feature composition for most frequent
       approach

    Args:
        ctt (CTT()): CttTax object
        buckets (defaultdict(list)): Group number mapped to list of tuples
                                     containing literal code token type and
                                     Jaccard similarity regarding reference
                                     code token type
    Return
        stats  defaultdict(defaultdict(int): Maps group to a mapping of
                                             features and frequency
    """
    stats = defaultdict(lambda: defaultdict(int))
    for group, tax_and_js in buckets.items():
        lit_tax = list()
        for tax in tax_and_js:
            lit_tax.append(ast.literal_eval(tax[0]))

        for tax in lit_tax:
            for cat_idx, feature in enumerate(tax):
                stats[str(group)][str(cat_idx) + "-" + str(feature)] += 1

    return stats


def analyze_group_feature_composition_clustering(ctt, df=None, encoder_path=None):
    """Analyzes code token type groups' feature composition for clustering
       approach

    Args:
        ctt (CTT()): CttTax object
        encoder_path (Path): The name of the file that contains taxonomy
                             encodings
    Return
        stats  defaultdict(defaultdict(int): Maps group to a mapping of
                                             features and frequency
    """
    stats = defaultdict(lambda: defaultdict(int))

    if encoder_path:
        with open(encoder_path, "r") as f:
            data = json.load(f)
            for encoding in data["encoding"].items():
                lit_tax = ctt.translate_to_literal(ast.literal_eval(encoding[0]))
                for cat_idx, feature in enumerate(lit_tax):
                    stats[encoding[1]][str(cat_idx) + "-" + str(feature)] += 1
        return stats
    else:
        for g, v in zip(df["group"], df["vec"]):
            # print(v)
            lit_tax = ctt.translate_to_literal(ctt.to_two_d(ctt.add_padding(v)))
            for cat_idx, feature in enumerate(lit_tax):
                stats[str(g)][str(cat_idx) + "-" + str(feature)] += 1
        return stats


def jaccard_similarity(tax_a, tax_b):
    """Computes Jaccard similarity of two code token types.

    Args:
        tax_a (list(list)): First code token type
        tax_b (list(list)): Second code token type
    Return
        mean(js) (float): Jaccard similarity of both code token types.
    """
    tax_a = ast.literal_eval(tax_a)
    tax_b = ast.literal_eval(tax_b)
    js = [float(len(set(a).intersection(set(b))) / len(set(a).union(set(b))))
          if a or b else 0. for a, b in zip(tax_a, tax_b)]
    return mean(js)


def pearson(group, vec, df):
    """Computes mean pairwise Pearson correlation of code token types
       in dataset.

    Args:
        group (int): code token type's group
        vecs (list): numerical code token type vectors with padding
        df (dataframe): Columns: group, numerical code token type vector,
                        literal code token type
    Return
        mean_score (float): Code token type' mean pairwise Pearson correlation
    """
    sim_score = []
    for i in df["vec"][(df["group"] == group)]:
        if i != vec:
            corr, _ = pearsonr(vec, i)
            sim_score.append(corr)
    try:
        mean_score = mean(sim_score)
    except StatisticsError:
        mean_score = 1.
    return mean_score


def similarity(group, tax, df):
    """Computes mean pairwise Jaccard similarity of code token types
       in dataset.

    Args:
        group (int): code token type's group
        vecs (list): numerical code token type vectors with padding
        df (dataframe): Columns: group, numerical code token type vector,
                        literal code token type
    Return
        mean_score (float): Code token type' mean pairwise Jaccard similarity
    """
    sim_score = []
    for i in df["tax"][(df["group"] == group) & (df["tax"] != tax)]:
        sim_score.append(jaccard_similarity(tax, i))
    try:
        mean_score = mean(sim_score)
    except StatisticsError:
        mean_score = 1.
    return mean_score


def write_20_most_frequent_code_token_types_to_file(df):
    """Writes 20 most frequent code token types to file.

    Args:
        df (dataframe): Contains columns literal code token types and
                        frequency
    """
    with open("ctt_strings.txt", "w") as f:
        f.write("total number of code token types: "
                + str(df.shape[0]) + "\n")
        f.write("enabled categories: " + str(categories) + "\n\n\n")
        f.write("20 MOST FREQUENT CODE TOKEN TYPES" + "\n\n")
        for i, tax in enumerate(df[:20]["taxonomy"]):
            f.write(str(i) + " " + tax + "\n\n")
        f.write("\n\n")


def write_20_most_frequent_related_code_tokens_to_file(df, tax_to_tokens):
    """Writes 20 most frequent code tokens of the 20 most frequent
       code token types to file.

    Args:
        df (dataframe): Contains columns literal code token types and
                        frequency
        tax_to_tokens (defaultdict(defaultdict(int))): literal code token types
                                                       mapped to their related
                                                       code tokens
    """
    with open("ctt_strings.txt", "a") as f:
        f.write("20 MOST FREQUENT RELATED CODE TOKENS" + "\n\n")
        for i, tax in enumerate(df[:20]["taxonomy"]):
            f.write(str(i) + " ")
            sorted_tax_to_tokens = sorted(tax_to_tokens[tax].items(),
                                          key=lambda x: x[1],
                                          reverse=True)[:20]
            f.write(str(sorted_tax_to_tokens))
            f.write("\n\n")
        f.write("\n")


def write_unknown_code_tokens_to_file(total_num_code_tokens,
                                      unknown_num_code_tokens,
                                      unknown_code_tokens):
    """Writes code tokens assigned to a code token type with an unknown
       syntax type to file.

    Args:
        total_num_code_tokens (int): total number of code tokens in dataset
        unknown_num_code_tokens (int): total number of code token types with
                                       an unknown syntax type
        unknown_code_tokens (defaultdict(int)): code tokens assigned to
                                                a code token type with an
                                                unknown syntax type mapped to
                                                their frequency
    """
    with open("ctt_strings.txt", "a") as f:
        f.write("znknown_frequency: " + str(unknown_num_code_tokens) + "\n\n")
        f.write("znknown_code_tokens: "
                + str(sorted(unknown_code_tokens.items(),
                             key=lambda x: x[1],
                             reverse=True)) + "\n\n")
        f.write("znknown_ratio: "
                + str(unknown_num_code_tokens / total_num_code_tokens) + "\n\n")
        f.write("total_num_code_tokens: "
                + str(total_num_code_tokens) + "\n\n")
        f.write("\n\n")


def most_frequent_to_df(ctt, buckets):
    """Puts results of group_by_20_most_frequent_code_token_types into a
       a dataframe and adds numerical code token type vectors without padding
       for further analysis.

    Args:
        ctt (CTT()): CttTax object
        buckets (defaultdict(list)): Group number mapped to list of tuples
                                     containing literal code token type and
                                     Jaccard similarity regarding reference
                                     code token type
    Return
        df (dataframe): Columns: group,
                        numerical code token type vectors without padding,
                        liter code token types, Jaccard similarity
    """
    groups = list()
    new_vecs = list()
    literal_tax = list()
    jaccard_sim = list()
    for g, b in buckets.items():
        for lit_tax, js in b:
            groups.append(g)
            literal_tax.append(lit_tax)
            jaccard_sim.append(js)
            new_vecs\
                .append(ctt
                        .remove_padding(ctt
                                        .flatten(ctt
                                                 .translate_to_vec(lit_tax))))
    df = pd.DataFrame(list(zip(groups,
                               new_vecs,
                               literal_tax,
                               jaccard_sim)),
                      columns=["group", "vec", "tax", "js"])
    return df


def plot_frequency(df):
    """Plots 20 most frequent code token type's frequncy.

    Args:
        df (dataframe): Contains columns literal code token types and
                        frequency
    """
    fig = plt.figure()
    df[:20].plot.bar(x="taxonomy", y="frequency", rot=0)
    y_pos = range(20)
    plt.xticks(y_pos, range(20), rotation=0)
    fig.tight_layout()
    plt.savefig("ctt_frequency.png")


def plot_ctt_group_analysis(stats, dir, filename, num_plots):
    """Plots results of  group feature composition analysis.

    Args:
        stats  defaultdict(defaultdict(int): Maps group to a mapping of
                                             features and frequency
        dir (Path): Directory where to save the plots.
        filename (str): Name of plot. Group number is added automatically.
    """
    num_groups = 20
    rows = (num_plots + 1) // 2
    cols = 2 if num_plots > 1 else 1
    top_n = 3

    for group in range(num_groups):
        stats_by_category = sorted(stats[str(group)].items(),
                                   key=lambda x: int(x[0].split("-")[0]),
                                   reverse=False)

        num_feats_per_cat = defaultdict(int)
        for stat in stats_by_category:
            num_feats_per_cat[str(stat[0].split("-")[0])] += 1
        features = [stat[0].split("-")[1] for stat in stats_by_category]
        feature_freqs = [stat[1] for stat in stats_by_category]

        for i in range(rows):
            for j, l in zip(range(cols), ["upper left", "lower right"]):
                ax = plt.subplot2grid((rows, cols), (i, j))

                idx = i * cols + j

                if idx < num_plots:
                    start = sum([n for n in num_feats_per_cat.values()][:idx])
                    offset = start + num_feats_per_cat[str(idx)]

                    sorted_idx_freq = np.argsort(feature_freqs[start:offset])[::-1]
                    freqs = list(np.asarray(feature_freqs[start:offset])
                                 [sorted_idx_freq])
                    labels = list(np.asarray(features[start:offset])
                                  [sorted_idx_freq])

                    patches, texts, autotexts = ax.pie(freqs,
                                                       autopct=my_autopct,
                                                       shadow=False,
                                                       startangle=90,
                                                       textprops={'fontsize': 20})
                    plt.legend(labels[:top_n],
                               loc=l,
                               fontsize="20")
                    plt.title(categories[idx], fontsize="25")
                    plt.suptitle("Code token type group " + str(group),
                                 fontsize="30")
                else:
                    ax.remove()

        fig = plt.gcf()
        fig.set_size_inches(10.5, 10.5)
        plt.savefig(dir + "ctt_group_" + str(group) + filename + ".png",
                    bbox_inches='tight',
                    dpi=400)


# https://stackoverflow.com/questions/34035427/conditional-removal-of-labels-in-matplotlib-pie-chart/34035864
def my_autopct(pct):
    return ('%.2f' % pct) if pct > 5. else ''


if __name__ == '__main__':
    NUM_PROCS = 16
    data_path = "data/python50k_eval.json"
    # data_path = "data/python100k_train.json"
    tax_encoder_data_path = "data/encoder_tax.json"
    group_analysis_folder = "group_analysis/"
    most_frequent_dir = group_analysis_folder + "most_frequent/"
    clustering_dir = group_analysis_folder + "clustering/"

    if not os.path.isdir(group_analysis_folder):
        os.makedirs(group_analysis_folder)
        os.makedirs(group_analysis_folder + "most_frequent/")
        os.makedirs(group_analysis_folder + "clustering/")

    categories = ["SyntaxType", "Context", "Origin", "Length", "Frequency"]
    ctt = CttTax(categories)

    vec_to_tax, tax_freq, tax_to_tokens = categorize(data_path,
                                                     categories,
                                                     num_procs=NUM_PROCS)

    df = pd.DataFrame(tax_freq, columns=["taxonomy", "frequency"])

    vecs = list()
    new_vecs = list()
    for vec in vec_to_tax.keys():
        vec = ast.literal_eval(vec)
        vecs.append(vec)
        new_vecs.append(ctt.remove_padding(ctt.flatten(vec)))

    write_20_most_frequent_code_token_types_to_file(df)
    write_20_most_frequent_related_code_tokens_to_file(df, tax_to_tokens)

    plot_frequency(df)

    total_num_code_tokens, unknown_num_code_tokens, unknown_code_tokens\
        = unkown_syntax_type_analysis(tax_to_tokens)
    write_unknown_code_tokens_to_file(total_num_code_tokens,
                                      unknown_num_code_tokens,
                                      unknown_code_tokens)

    buckets = group_by_20_most_frequent_code_token_types(df)
    df_most_freq = most_frequent_to_df(ctt, buckets)
    df_group_sim_most_freq = group_similarity_analysis(df_most_freq, tax_freq)

    stats = analyze_group_feature_composition_most_frequent(ctt, buckets)
    plot_ctt_group_analysis(stats, most_frequent_dir, "_most_frequent", len(categories))

    # if no tax_encoder file exists
    # df_clustering = clustering(vec_to_tax, vecs, new_vecs)

    # if tax_encoder file exists
    df_clustering = get_clustering_from_encoder_file(tax_encoder_data_path,
                                                     vec_to_tax, vecs,
                                                     new_vecs)

    df_group_sim_clustering = group_similarity_analysis(df_clustering,
                                                        tax_freq)
    pca_analysis(ctt, df_clustering, new_vecs)

    # if no tax_encoder file exists
    # stats = analyze_group_feature_composition_clustering(ctt,
    #                                                      df_clustering)

    # if tax_encoder file exists
    stats = analyze_group_feature_composition_clustering(ctt,
                                                         df=None,
                                                         encoder_path=tax_encoder_data_path)

    plot_ctt_group_analysis(stats, clustering_dir, "_clustering", len(categories))
