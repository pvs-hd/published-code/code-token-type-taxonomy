# Copyright (c) 2021 Gabriel Rashidi <g.rashidi@stud.uni-heidelberg.de>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import keyword
import builtins
from collections import OrderedDict
import ast
import cosy
from cosy.tree import *
from stdlib_list import stdlib_list
from statistics import mean
import re


class _BaseCategory(object):
    def __init__(self):
        """Base class for categories implementing shared attributes and
           methods.

        Args:
            None
        """
        self.ast_node_types = dir(ast)[:107] + ["defaults",
                                                "decorator_list",
                                                "ListLoad",
                                                "TupleLoad"]
        self.feature_vec_len = None
        self.conditions = OrderedDict()
        self.taxed = 0
        self.add_conditions()

    def set_feature_vec_len(self, len):
        """Set feature vector length of the category. Equals the number of
           featres in the particular category.
        Args:
            int: Feature length.

        Returns:
            (void)
        """
        self.feature_vec_len = len

    def get_features(self):
        """Get the features of the particular category.
        Args:
            None

        Returns:
            (list) List with the category's features
        """
        return ["_".join(x.split('_')[1:])
                for x in dir(self) if x.split('_')[0] == "is"]

    def add_conditions(self):
        """Add the conditions to check (feature methods) of the particular
           category to the conditions attribute.
        Args:
            None

        Returns:
            (void)
        """
        condition_methods = [x for x in dir(self) if x.split("_")[0] == "is"]
        for i, method in enumerate(condition_methods):
            self.conditions[i] = getattr(self, method)

    def get_feature_vec(self, *args):
        """Get the feature vector. Combining the feature vectors of different
           categories represent a code token's code token type.
        Args:
            as needed ; usually --> (vertex (igraph.Vertex), int): node,
                                                                   node's index

        Returns:
            (list(int)) 1D feature vector
        """
        self.taxed = 0
        feature_vector = [0] * self.feature_vec_len
        for vec_pos, con in self.conditions.items():
            if con(*args):
                feature_vector[vec_pos] = 1 * con(*args)
                self.taxed = 1
        return feature_vector

    def get_ancestors(self, node):
        """Root path wihtout node itself.
        Args:
            vertex (igraph.Vertex): The vertex to investigate.

        Returns:
            (list(vertex (igraph.Vertex))) Ancestors of node
        """
        return root_path(node)[1:]

    def get_ancestor_types(self, node):
        """Root path wihtout node itself.
        Args:
            vertex (igraph.Vertex): The vertex to investigate.

        Returns:
            (list(str) Ancestor node's node types
        """
        return [n["symbol"] for n in root_path(node)[1:]]

    def get_leftmost_childindex_of_ancestor_call(self, node):
        """Get the first ancstor call's first child's index.
        Args:
            vertex (igraph.Vertex): The vertex to investigate.

        Returns:
            (int) Index first ancstor call's first child
        """
        return children(self.get_ancestors(node)[self
                                                 .get_ancestor_types(node)
                                                 .index("Call")])[0].index


class SyntaxType(_BaseCategory):
    """Category checks syntax type of AST node.

    Args:
        None
    """

    def __init__(self):
        super().__init__()
        self.python_keywords = set(keyword.kwlist + ["self"])
        self.exceptions = [name for name, value in builtins.__dict__.items()
                           if isinstance(value, type) and issubclass(value,
                                                                     BaseException)]

    def get_index_first_attribute_load(self, node):
        """Get the index of  the first "AttributeLoad" AST node in the ancestors
           of the given node.

        Args:
            vertex (igraph.Vertex): The vertex to investigate.

        Returns:
            (int) Index of first "AttributeLoad" AST node in the ancestors.
        """
        return self.get_ancestors(node)[self.get_ancestor_types(node)
                                        .index("AttributeLoad")].index

    def is_arg_def(self, *args):
        """Check if AST node is an argument definition

            A:  no other feature has already been True
                -->(security measure; prevents double categorizations),
                not root node,
                has nodetype: Name*,
                not exception,
                not python_keyword,
            AND

            B:  node's parent's symbol is "NameParam"
                (parameter in func def or method def)
            OR

            C:  is leaf,
                vararg in node's ancestor types
                (vararg)

            OR

            D:  is leaf,
                kwarg in node's ancestor types
                (kwarg)

        Args:
            as needed ; usually --> (vertex (igraph.Vertex), int): node,
                                                                   node's index

        Returns:
            (bool) True if the AST node is an argument definition.
        """
        node, node_index, _ = args

        return self.taxed == 0\
            and is_leaf(node)\
            and not is_root(node)\
            and "Name" in parent(node)["symbol"]\
            and node["symbol"] not in self.exceptions\
            and node["symbol"] not in self.python_keywords\
            and\
            (
                parent(node)["symbol"] == "NameParam"
            )\
            or\
            (is_leaf(node) and "vararg"
             in self.get_ancestor_types(node))\
            or\
            (is_leaf(node) and "kwarg"
             in self.get_ancestor_types(node))

    def is_attribute(self, *args):
        """Check if AST node is an attribute
           (e.g. qualifier.an_attribute)

           A:   no other feature has already been True
                -->(security measure; prevents double categorizations),
                not root node,

           AND

           (
           B:   leaf node,
                has attr in ancestors,
                has no Call in ancestors

           OR

           C:   leaf node,
                has attr in ancestors,
                first AttributeLoad in ancestors is not the first child of the
                first Call in ancestors
           )


        Returns:
            (bool) True if AST node is a attribute false otherwise
        """
        node, _, _ = args

        return self.taxed == 0\
            and not is_root(node)\
            and ((is_leaf(node) and "attr" in self.get_ancestor_types(node)
                  and "Call" not in self.get_ancestor_types(node))
                 or
                 (is_leaf(node) and "attr" in self.get_ancestor_types(node)
                  and "Call" in self.get_ancestor_types(node)
                  and not
                  self.get_leftmost_childindex_of_ancestor_call(node)
                  == self.get_index_first_attribute_load(node)))

    def is_class_def(self, *args):
        """Check if an AST node is a class definition

        Args:
            as needed ; usually --> (vertex (igraph.Vertex), int): node,
                                                                   node's index

        Returns:
            (bool) True if AST node is a class definition, false otherwise
        """
        node, _, _ = args

        return self.taxed == 0\
            and is_leaf(node)\
            and not is_root(node)\
            and parent(node)["symbol"] == "ClassDef"

    def is_class_usg(self, *args):
        """Check if an AST node is a class usage.

            A:   no other feature has already been True
                -->(security measure; prevents double categorizations),
                not root node
            AND
            (
                B:  has "Name" in parent node and "bases" in ancestor types
                    (base class, e.g. def AnotherClass(BaseClassUsage))
                OR
                (
                    C:  not python keyword,
                        not exception,
                        "Call" in ancestor node types
                    AND
                    (
                        D:   NameLoad in ancestor node types
                            (e.g. a_var = ClassUage())
                        OR
                        E:   first "AttributeLoad" is the first Call's leftmost child
                            (e.g. Lib.ClassUsage())
                    )
                    AND
                    F   :   first char is an upper case
                )
                OR
                (
                    G:  has nodetype: Name*,
                        not exception,
                        not python_keyword,
                        left subtree of last Attribute* in ancestor nodes without
                            Call node as parent (for distinguishing with func call),
                        first char is an upper case
                        (e.g. a_var = ClassUsage.an_attribute
                        or resutls = ClassUsage.a_method())
                )
            )
            A and (B or (c and (D or E) and F) or G)
        Args:
            as needed ; usually --> (vertex (igraph.Vertex), int): node,
                                                                   node's index

        Returns:
            (bool) True if AST node is a class usage,false otherwise.
        """
        node, _, _ = args

        return self.taxed == 0\
            and is_leaf(node)\
            and not is_root(node)\
            and\
            (
                ("Name" in parent(node)["symbol"]
                    and "bases" in self.get_ancestor_types(node))
                or
                (
                    not node["symbol"] in self.python_keywords
                    and not node["symbol"] in self.exceptions
                    and "Call" in self.get_ancestor_types(node)
                    and
                    (
                        ("NameLoad" in self.get_ancestor_types(node))
                        or
                        ("attr" in self.get_ancestor_types(node)
                         and (self.get_leftmost_childindex_of_ancestor_call(node)
                              == self.get_index_first_attribute_load(node)))
                    )
                    and node["symbol"][0].isupper()
                )
                or
                (
                    ("Name" in parent(node)["symbol"])
                    and node["symbol"] not in self.exceptions
                    and node["symbol"] not in self.python_keywords
                    and
                    ("Attribute" in "".join(self.get_ancestor_types(node))
                     and node["symbol"] in [v["symbol"] for c in
                                            children([n for n in self.get_ancestors(node)
                                                     if "Attribute" in n["symbol"]][-1])[:-1]
                                            for v in leafs(c)]
                     and parent(parent(node))["symbol"] != "Call")
                    and node["symbol"][0].isupper()
                )
            )

    def is_const_num(self, *args):
        """Check if AST node is a constant number (e.g. 5)

        Args:
            as needed ; usually --> (vertex (igraph.Vertex), int): node,
                                                                   node's index

        Returns:
            (bool) True if AST node is a constant false otherwise
            """
        node, _, _ = args

        return self.taxed == 0\
            and is_leaf(node)\
            and not is_root(node)\
            and parent(node)["symbol"] == "Num"

    def is_const_str(self, *args):
        """Check if AST node is a literal (string constant)

        Args:
            as needed ; usually --> (vertex (igraph.Vertex), int): node,
                                                                   node's index

        Returns:
            (bool) True if AST node is a literal false otherwise
        """
        node, _, _ = args

        return self.taxed == 0\
            and is_leaf(node)\
            and not is_root(node)\
            and parent(node)["symbol"] == "Str"

    def is_exception(self, *args):
        """Check if AST node is an exception (e.g. except someException)

        Args:
            as needed ; usually --> (vertex (igraph.Vertex), int): node,
                                                                   node's index

        Returns:
            (bool) True if AST node is an exception false otherwise
        """
        node, _, _ = args

        return self.taxed == 0\
            and is_leaf(node)\
            and node["symbol"] in self.exceptions

    def is_func_def(self, *args):
        """Check if AST node is function def

           no other feature has already been True
           -->(security measure; prevents double categorizations),
           leaf node,
           not root node,
           node's parent has symbol "FunctionDef",
           node is parent's node first child,
           not "ClassDef" is in ancestor node types

        Args:
            as needed ; usually --> (vertex (igraph.Vertex), int): node,
                                                                   node's index

        Returns:
            (bool) True if AST node is function def false otherwise
        """
        node, _, _ = args

        return self.taxed == 0\
            and is_leaf(node)\
            and not is_root(node)\
            and parent(node)["symbol"] == "FunctionDef"\
            and node == children(parent(node))[0]\
            and "ClassDef" not in self.get_ancestor_types(node)

    def is_func_usg(self, *args):
        """Check if AST node is a function call

           no other feature has already been True
           -->(security measure; prevents double categorizations),
           leaf node,
           not root node,
           "NameLoad" in ancestor node types,
           "Call" in ancestor node types,
           node is the first child of the first Call in ancestors
           first char of node's symbol is not upper case


        Returns:
            (bool) True if AST node is a function call false otherwise
        """
        node, _, _ = args

        return self.taxed == 0\
            and is_leaf(node)\
            and not is_root(node)\
            and "NameLoad" in self.get_ancestor_types(node)\
            and "Call" in self.get_ancestor_types(node)\
            and parent(node).index\
            == self.get_leftmost_childindex_of_ancestor_call(node)\
            and not node["symbol"][0].isupper()

    def is_imp_alias(self, *args):
        """Check if AST node is an import ID (i.e. alias, e.g. import some_lib as imp_alias)

        Args:
            as needed ; usually --> (vertex (igraph.Vertex), int): node,
                                                                   node's index

        Returns:
            (bool) True if AST node is an import alias false otherwise
        """
        node, _, _ = args

        return self.taxed == 0\
            and is_leaf(node)\
            and "identifier" in self.get_ancestor_types(node)\
            and "alias" in self.get_ancestor_types(node)

    def is_imp_lib(self, *args):
        """Check if AST node is a module (i.e. imported lib)

           no other feature has already been True
           -->(security measure; prevents double categorizations),
           leaf node,
           not root node

           AND

           (
           "Import" is in ancestor node types
           OR
           "ImportFrom" is in ancestor node types
           )

           AND NOT

           (
           node is a sub import
           OR
           node is an import ID (i.e. import alias)
           )

        Args:
            as needed ; usually --> (vertex (igraph.Vertex), int): node,
                                                                   node's index

        Returns:
            (bool) True if AST node is a module false otherwise
        """
        node, _, vars_def = args

        return self.taxed == 0\
            and is_leaf(node)\
            and not is_root(node)\
            and\
            ("Import" in self.get_ancestor_types(node)
             or
             "ImportFrom" in self.get_ancestor_types(node))\
            and not (self.is_imp_sublib(node, node.index, vars_def)
                     or self.is_imp_alias(node, node.index, vars_def))

    def is_imp_sublib(self, *args):
        """Check if AST node is a sub import
           (e.g. from some_lib import imp_sublib)

        Args:
            as needed ; usually --> (vertex (igraph.Vertex), int): node,
                                                                   node's index

        Returns:
            (bool) True if AST node is sub import false otherwise
        """
        node, _, _ = args

        return self.taxed == 0\
            and is_leaf(node)\
            and "alias" in self.get_ancestor_types(node)\
            and "ImportFrom" in self.get_ancestor_types(node)

    def is_keyword(self, *args):
        """Check if AST node is python keyword (e.g. for)

        Args:
            as needed ; usually --> (vertex (igraph.Vertex), int): node,
                                                                   node's index

        Returns:
            (bool) True if AST node is python keyword false otherwise
        """
        node, _, _ = args

        return self.taxed == 0\
            and is_leaf(node)\
            and node["symbol"] in self.python_keywords

    def is_method_def(self, *args):
        """Check if AST node is method def

           no other feature has already been True
           -->(security measure; prevents double categorizations),
           leaf node,
           not root node,
           node's parent has symbol "FunctionDef",
           node is parent's node first child,
           "ClassDef" is in ancestor node types

        Args:
            as needed ; usually --> (vertex (igraph.Vertex), int): node,
                                                                   node's index

        Returns:
            (bool) True if AST node is method def false otherwise
        """
        node, _, _ = args

        return self.taxed == 0\
            and is_leaf(node)\
            and not is_root(node)\
            and parent(node)["symbol"] == "FunctionDef"\
            and node == children(parent(node))[0]\
            and 'ClassDef' in self.get_ancestor_types(node)

    def is_method_usg(self, *args):
        """Check if AST node is a method call

           no other feature has already been True
           -->(security measure; prevents double categorizations),
           leaf node,
           not root node,
           "attr" in ancestor node types,
           "Call" in ancestor node types,
           first "AttributeLoad" is the first Call's leftmost child
           first char of node's symbol is not upper case


        Returns:
            (bool) True if AST node is a method call false otherwise
        """
        node, _, _ = args

        return self.taxed == 0\
            and is_leaf(node)\
            and not is_root(node)\
            and "attr" in self.get_ancestor_types(node)\
            and "Call" in self.get_ancestor_types(node)\
            and (self.get_leftmost_childindex_of_ancestor_call(node)
                 == self.get_index_first_attribute_load(node))\
            and not node["symbol"][0].isupper()

    def is_var_def(self, *args):
        """Check if AST node is a var definition.

            A:  no other feature has already been True
                -->(security measure; prevents double categorizations),
                not root node,
                not exception,
                not python_keyword,
                node index in vars_def (i.e. a dict of vars def in the file)

            OR

            B:  is leaf,
                Global in node's ancestor types,
                (global variable)

        Args:
            as needed ; usually --> (vertex (igraph.Vertex), int): node,
                                                                   node's index

        Returns:
            (bool) True if AST node is a var definition, false otherwise.
        """
        node, _, vars_def = args

        return self.taxed == 0\
            and is_leaf(node)\
            and not is_root(node)\
            and node["symbol"] not in self.exceptions\
            and node["symbol"] not in self.python_keywords\
            and node.index in vars_def\
            or\
            (is_leaf(node) and "Global" in self.get_ancestor_types(node))

    def is_var_usg(self, *args):
        """Check if AST node is a var usage.

            A:  no other feature has already been True
                -->(security measure; prevents double categorizations),
                not root node,
                has nodetype: Name*,
                not exception,
                not python_keyword,

           AND

           (
           B:   Call is in ancestor node types,
                not attr is in ancestor node types,
                node is among first Call's children' leafs excluding the
                first child
                (parameter in func call)

           OR

           C:   not Call is node's ancestor types,
                not Subscript* in node's ancestor types,
                not bases in node's ancestor types,
                not Attribute* in node's ancestor types
                not a var definition
                (local variable)

           OR

           D:   Subscript* in node's ancestor types,
                not Attribute* in node's ancestor types
                (Subscripts)

            OR

           E:   first char is not an upper case
                left subtree of last Attribute* in ancestor nodes without
                Call node as parent (for distinguishing with func call)
                (from pre_attribute)
           )

           OR

           F:   no other feature has already been True
                -->(security measure; prevents double categorizations),
                not root node,
                parent's type is keyword

           A and (B or C or D or E) or F

        Args:
            as needed ; usually --> (vertex (igraph.Vertex), int): node,
                                                                   node's index

        Returns:
            (bool) True if AST node is a var definition, false otherwise.
        """
        node, _, vars_def = args

        return self.taxed == 0\
            and is_leaf(node)\
            and not is_root(node)\
            and "Name" in parent(node)["symbol"]\
            and node["symbol"] not in self.exceptions\
            and node["symbol"] not in self.python_keywords\
            and\
            (
                ("Call" in self.get_ancestor_types(node)
                 and "attr" not in self.get_ancestor_types(node)
                 and node["symbol"] in [v["symbol"] for c in
                                        children([n for n in
                                                  self.get_ancestors(node)
                                                  if "Call" in n["symbol"]][0])[1:]
                                        for v in leafs(c)])
                or
                ("Call" not in self.get_ancestor_types(node)
                 and "Subscript" not in
                 "".join(self.get_ancestor_types(node))
                 and "bases" not in self.get_ancestor_types(node)
                 and "Attribute"
                 not in "".join(self.get_ancestor_types(node))
                 and not self.is_var_def(node, node.index, vars_def))
                or
                ("Subscript" in "".join(self.get_ancestor_types(node))
                 and "Attribute"
                 not in "".join(self.get_ancestor_types(node)))
                or
                ((not node["symbol"][0].isupper())
                 and
                 ("Attribute" in "".join(self.get_ancestor_types(node))
                 and node["symbol"] in [v["symbol"] for c in
                                        children([n for n in self.get_ancestors(node)
                                                 if "Attribute" in n["symbol"]][-1])[:-1]
                                        for v in leafs(c)]
                 and parent(parent(node))["symbol"] != "Call"))
            )\
            or\
            (
                self.taxed == 0
                and is_leaf(node)
                and not is_root(node)
                and parent(node)["symbol"] == "keyword"
            )

    def is_yqualifier(self, *args):
        """Check if AST node is a qualifier (i.e. pre attribute)
           (e.g. qualifier.some_attribute)

           A:   no other feature has already been True
                -->(security measure; prevents double categorizations),
                leaf node,
                not root node,
                has nodetype: Name*,
                not exception,
                not python_keyword

          AND

          B:    left subtree of last Attribute* in ancestor nodes without
                Call node as parent


        Returns:
            (bool) True if AST node is a qualifier false otherwise
        """
        node, _, _ = args

        return self.taxed == 0\
            and is_leaf(node)\
            and not is_root(node)\
            and "Name" in parent(node)["symbol"]\
            and node["symbol"] not in self.exceptions\
            and node["symbol"] not in self.python_keywords\
            and\
            ("Attribute" in "".join(self.get_ancestor_types(node))
             and node["symbol"] in [v["symbol"] for c in
                                    children([n for n in self.get_ancestors(node)
                                              if "Attribute" in n["symbol"]][-1])[:-1]
                                    for v in leafs(c)]
             and parent(parent(node))["symbol"] != "Call")

    def is_znknown(self, *args):
        """Check if AST node is a leaf node with a code token
           that has not been cateorized
           (znknown so it is evaluated last)

        Args:
            as needed ; usually --> (vertex (igraph.Vertex), int): node,
                                                                   node's index

        Returns:
            (bool) True if AST node an uncategorized code tokfalse otherwise
        """
        node, _, _ = args
        return self.taxed == 0 and is_leaf(node)\
            and not node["symbol"] in self.ast_node_types


class Context(_BaseCategory):
    """Category checks context of AST node. Only context specfied in types is
       considered. Limit attribute limits the range of context considered.
       Feature vectors of this  category are not one-hot encoded.

    Args:
        None
    """

    def __init__(self):
        super().__init__()
        self.limit = 2
        self.types = ["ClassDef", "FunctionDef", "For", "While", "If",
                      "orelse", "With", "Return", "Assign", "Raise",
                      "ExceptHandler", "BinOpSub", "BinOpAdd", "BinOpSub",
                      "BinOpMult", "BinOpDiv", "BinOpFloorDiv", "BinOpMod",
                      "BinOpPow", "BinOpLShift", "BinOpRShift", "BinOpBitOr",
                      "BinOpBitXor", "BinOpBitAnd", "BinOpBitMatMult",
                      "BoolOpAnd", "BoolOpOr", "CompareEq", "CompareNotEq",
                      "CompareLt", "CompareLtE", "CompareGt", "CompareGtE",
                      "CompareIs", "CompareIsNot", "CompareIn", "CompareNotIn",
                      "TryExcept", "NameParam", "bases", "Call"]

    def is_in_class_def(self, *args):
        """Check if AST node is in a class def. Filters for node types in types
           and counts occurences.

        Args:
            as needed ; usually --> (vertex (igraph.Vertex), int): node,
                                                                   node's index

        Returns:
            (int) Count of class defs AST node is in.
        """
        node, _, _ = args

        return list(filter(lambda x: x in self.types,
                           self.get_ancestor_types(node)))[:self.limit].count("ClassDef")

    def is_in_func_def(self, *args):
        node, _, _ = args

        return list(filter(lambda x: x in self.types,
                           self.get_ancestor_types(node)))[:self.limit].count("FunctionDef")

    def is_in_for(self, *args):
        node, _, _ = args

        return list(filter(lambda x: x in self.types,
                           self.get_ancestor_types(node)))[:self.limit].count("For")

    def is_in_while(self, *args):
        node, _, _ = args

        return list(filter(lambda x: x in self.types,
                           self.get_ancestor_types(node)))[:self.limit].count("While")

    def is_in_if(self, *args):
        node, _, _ = args

        return list(filter(lambda x: x in self.types,
                           self.get_ancestor_types(node)))[:self.limit].count("If")

    def is_in_else(self, *args):
        node, _, _ = args

        return list(filter(lambda x: x in self.types,
                           self.get_ancestor_types(node)))[:self.limit].count("orelse")

    def is_in_with(self, *args):
        node, _, _ = args

        return list(filter(lambda x: x in self.types,
                           self.get_ancestor_types(node)))[:self.limit].count("With")

    def is_in_return(self, *args):
        node, _, _ = args

        return list(filter(lambda x: x in self.types,
                           self.get_ancestor_types(node)))[:self.limit].count("Return")

    def is_in_assign(self, *args):
        node, _, _ = args

        return list(filter(lambda x: x in self.types,
                           self.get_ancestor_types(node)))[:self.limit].count("Assign")

    def is_in_raise(self, *args):
        node, _, _ = args

        return list(filter(lambda x: x in self.types,
                           self.get_ancestor_types(node)))[:self.limit].count("Raise")

    def is_in_except(self, *args):
        node, _, _ = args
        return list(filter(lambda x: x in self.types,
                           self.get_ancestor_types(node)))[:self.limit].count("ExceptHandler")

    def is_in_arithmetic_op(self, *args):
        """Check if AST node is in a arithmetic operation. Filters for
           node types in types. Looks for BinOp* pattern and counts occurences.

        Args:
            as needed ; usually --> (vertex (igraph.Vertex), int): node,
                                                                   node's index

        Returns:
            (int) Count of arithmetic operations AST node is in.
        """
        node, _, _ = args
        pattern = re.compile("BinOp*")

        return sum(1 for type in list(filter(lambda x: x in self.types,
                                             self.get_ancestor_types(node)))[:self.limit]
                   if pattern.match(type))

    def is_in_bool_op(self, *args):
        node, _, _ = args
        pattern = re.compile("BoolOp*")

        return sum(1 for type in list(filter(lambda x: x in self.types,
                                             self.get_ancestor_types(node)))[:self.limit]
                   if pattern.match(type))

    def is_in_comparison(self, *args):
        node, _, _ = args
        pattern = re.compile("Compare*")

        return sum(1 for type in list(filter(lambda x: x in self.types,
                                             self.get_ancestor_types(node)))[:self.limit]
                   if pattern.match(type))

    def is_in_try(self, *args):
        """Check if AST node is in a try block. Filters for node types in types
           and then checks for:

           not ExceptHandler in filtered types,
           TryExcept in filtered types

        Args:
            as needed ; usually --> (vertex (igraph.Vertex), int): node,
                                                                   node's index

        Returns:
            (int) Count of try blocks AST node is in.
        """
        node, _, _ = args
        filtered_types = list(filter(lambda x: x in self.types,
                                     self.get_ancestor_types(node)))

        if "ExceptHandler" not in filtered_types and "TryExcept" in filtered_types[:self.limit]:
            return filtered_types[:self.limit].count("TryExcept")
        else:
            return 0

    def is_in_parameter(self, *args):
        """Check if AST node is in a parameter context. Filters for node types
           in types and then checks for:

           A:       is in Call,
                    Name* OR attr in node's ancestor types,
                    not in first branch of Call
                    (parameter function call)

           elif

           NameParam:  NameParam
                       (parameter function def)

           elif

           bases: bases in ancestors (base class in ClassDef)
                  (parameter base class)

        Args:
            as needed ; usually --> (vertex (igraph.Vertex), int): node,
                                                                   node's index

        Returns:
            (int) Count of parameter contexts AST node is in.
        """
        node, node_index, _ = args
        filtered_types = list(filter(lambda x: x in self.types,
                                     self.get_ancestor_types(node)))

        if (
            "Call" in self.get_ancestor_types(node)
            and ("Name" in "".join(self.get_ancestor_types(node))
                 or "attr" in self.get_ancestor_types(node))
            and parent(node).index
            !=
            self.get_leftmost_childindex_of_ancestor_call(node)
            and "Call" in filtered_types[:self.limit]
        ):
            return filtered_types[:self.limit].count("Call")
        elif "NameParam" in filtered_types[:self.limit]:
            return filtered_types[:self.limit].count("NameParam")
        elif "bases" in filtered_types[:self.limit]:
            return filtered_types[:self.limit].count("bases")
        else:
            return 0


class Origin(_BaseCategory):
    """Category checks origin of AST node's symbol and distguishes between
       pythons standard library, external libaries (all libraries not in the
       standard library pool), declared within current AST (in file) and
       python's built-in tokens.

    Args:
        None
    """

    def __init__(self):
        super().__init__()
        self.python_keywords = set(keyword.kwlist + ["self"])
        self.ast = None
        self.stdlibs = None
        self.extlibs = None

    def get_stdlibs(self):
        """Get python's standard libraries in current ast

        Args:
            None

        Returns:
            (list(str)) List of python's standard libraries in
                        current ast attribute
        """
        in_file_imports = set([node["symbol"] for node
                               in cosy.tree.dfs(self.ast)
                               if is_leaf(node)
                               and ("Import" in self.get_ancestor_types(node)
                                    or "ImportFrom" in self.get_ancestor_types(node))])
        return list(in_file_imports.intersection(set(stdlib_list())))

    def get_extlibs(self):
        """Get python's external libraries in current ast

        Args:
            None

        Returns:
            (list(str)) List of python's external libraries in
                        current ast attribute
        """
        in_file_imports = set([node["symbol"] for node
                               in cosy.tree.dfs(self.ast)
                               if is_leaf(node)
                               and ("Import" in self.get_ancestor_types(node)
                                    or "ImportFrom" in self.get_ancestor_types(node))])
        return list(in_file_imports.difference(set(stdlib_list())))

    def get_pre_attrs(self, node):
        """Get AST node's pre attributes
           (e.g. pre_attribute1.pre_attribute2.attribute)

        Args:
            vertex (igraph.Vertex): The vertex to investigate.

        Returns:
            (set(str)) Set with nodes joined preattribute(s)
        """
        try:
            attributes = [".".join([n["symbol"]
                          for n in
                          leafs(children(parent(parent(node)))[0])])]
        except AssertionError:
            attributes = []

        return set(attributes)

    def is_from_stdlib(self, *args):
        """Check if AST node is from a standard library

        Args:
            as needed ; usually --> (vertex (igraph.Vertex), int): node,
                                                                   node's index

        Returns:
            (bool) True if AST node from standard library false otherwise
        """
        node, _, _ = args
        return self.taxed == 0\
            and is_leaf(node)\
            and\
            (node["symbol"] in self.stdlibs
             or not set(self.stdlibs).isdisjoint(self.get_pre_attrs(node)))

    def is_from_extlib(self, *args):
        """Check if AST node is from an external library

        Args:
            as needed ; usually --> (vertex (igraph.Vertex), int): node,
                                                                   node's index

        Returns:
            (bool) True if AST node from an external library false otherwise
        """
        node, _, _ = args
        return self.taxed == 0\
            and is_leaf(node)\
            and\
            (node["symbol"] in self.extlibs
             or not set(self.extlibs).isdisjoint(self.get_pre_attrs(node)))

    def is_from_infile(self, *args):
        """Check if AST node not from the standard library and not from
           the external library and not from the built-in library

        Args:
            as needed ; usually --> (vertex (igraph.Vertex), int): node,
                                                                   node's index

        Returns:
            (bool) True if AST node is from within source code file
                   false otherwise
        """
        node, node_index, vars_def = args
        return self.taxed == 0\
            and is_leaf(node)\
            and not self.is_from_stdlib(node, node_index, vars_def)\
            and not self.is_from_extlib(node, node_index, vars_def)\
            and not self.is_from_builtin(node, node_index, vars_def)

    def is_from_builtin(self, *args):
        """Check if AST node is built-in

        Args:
            as needed ; usually --> (vertex (igraph.Vertex), int): node,
                                                                   node's index

        Returns:
            (bool) True if AST node is built-in false otherwise
        """
        node, _, _ = args
        return self.taxed == 0\
            and node["symbol"] in self.python_keywords


class Length(_BaseCategory):
    """Category checks length of AST node's symbol and distguishes between
       short, medium and long

    Args:
        None
    """

    def is_short(self, *args):
        node, _, _ = args
        return len(node["symbol"]) <= 3

    def is_medium(self, *args):
        node, _, _ = args
        return len(node["symbol"]) > 3 and len(node["symbol"]) <= 10

    def is_long(self, *args):
        node, _, _ = args
        return len(node["symbol"]) > 10


class Frequency(_BaseCategory):
    """Category checks frequency of AST node's symbol and distguishes between
       low frequent, medium frequent and high frequent

    Args:
        None
    """

    def __init__(self):
        super().__init__()
        self.ast = None
        self.boundary_0 = None
        self.boundary_1 = None
        self.boundary_2 = None
        self.frequencies = None

    def get_frequencies(self):
        """Count frequencies of each AST node in current AST.

        Args:
            None

        Returns:
            (defaultdict(int)) Frequencies of each AST node
        """
        frequencies = defaultdict(int)
        for v in cosy.tree.dfs(self.ast):
            if is_leaf(v) and v["symbol"] not in self.ast_node_types:
                frequencies[v["symbol"]] += 1
        self.frequencies = frequencies

    def set_boundaries(self):
        """Determine boundaries for the three frequency features depending
           on the frequencies of the AST nodes in current AST

        Args:
            None

        Returns:
            (void)
        """
        sorted_frequencies = sorted(self.frequencies.items(),
                                    key=lambda x: x[1],
                                    reverse=True)
        if sorted_frequencies:
            mean_freq = int(mean(list(set([x[1]
                                      for x in sorted_frequencies]))))
            self.boundary_0 = sorted_frequencies[-1][1]
            self.boundary_1 = int((mean_freq - self.boundary_0) / 2)
            self.boundary_2 = int((sorted_frequencies[0][1] - mean_freq) / 2)
        else:
            self.boundary_0 = 0
            self.boundary_1 = 0
            self.boundary_2 = 0

    def is_low_frequent(self, *args):
        node, _, _ = args
        freq = self.frequencies[node["symbol"]]
        return freq >= self.boundary_0 and freq < self.boundary_1

    def is_medium_frequent(self, *args):
        node, _, _ = args
        freq = self.frequencies[node["symbol"]]
        return freq >= self.boundary_1 and freq < self.boundary_2

    def is_high_frequent(self, *args):
        node, _, _ = args
        freq = self.frequencies[node["symbol"]]
        return freq >= self.boundary_2
