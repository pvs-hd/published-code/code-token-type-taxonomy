import cosy
from cosy.tree import children


class VarsCollector():
    def __init__(self, tree):
        self.tree = tree
        self.context = [('global', set())]
        self.avail_vars = set()
        self.vars_def = {}
    
    def collect_vars(self):
        for node in self.tree:
            if node["symbol"] == "ClassDef":
                self.context.append(('class', set()))
            if node["symbol"] == "FunctionDef":
                self.context.append(('function', set()))
            if node["symbol"] == "decorator_list":
                removed_vars = set()
                if self.context:
                    removed_vars.update(self.context[-1][1])
                    self.context.pop()
                self.avail_vars.difference_update(removed_vars)
            if node["symbol"] == "Global":
                for child in children(node):
                    if children(child):
                        glb_var = children(child)[0]
                        if self.context: # just to be sure!
                            self.context[0][1].add(glb_var["symbol"])
                            self.avail_vars.add(glb_var["symbol"])
                            self.vars_def[glb_var.index] = (glb_var["symbol"], self.context[0][0])
            if node["symbol"] == "Assign":
                if children(node):
                    left_side = children(node)[0]
                    targets = []
                    if left_side["symbol"] == "TupleStore":
                        for child in children(left_side):
                            targets.append(child)
                    if left_side["symbol"] == "NameStore":
                        targets.append(left_side)

                    for target in targets:
                        if children(target):
                            local_var = children(target)[0]
                            if local_var["symbol"] not in self.avail_vars:
                                if self.context:
                                    self.context[-1][1].add(local_var["symbol"])
                                    self.avail_vars.add(local_var["symbol"])
                                    self.vars_def[local_var.index] = (local_var["symbol"], self.context[-1][0])
            if (node["symbol"] == "For") or (node["symbol"] == "comprehension"):
                if children(node):
                    name_store = children(node)[0]
                    if children(name_store):
                        loop_idx = children(name_store)[0]
                        if self.context:
                            self.context[-1][1].add(loop_idx["symbol"])
                            self.avail_vars.add(loop_idx["symbol"])
                            self.vars_def[loop_idx.index] = (loop_idx["symbol"], self.context[-1][0])
