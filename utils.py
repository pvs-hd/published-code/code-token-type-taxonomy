import importlib




def dynamic_import(module_name, class_name):
    """Imports the class which provides handlers

    Args:
        module_name (str): Name of the module
        class_name (str): Name of the class

    Returns:
        (object) The imported class.
    """
    # by default, the import type is always GLOBAL
    if (module_name is None) or (class_name is None):
        return None

    if (module_name == "cosy") and (class_name == "cosy"):
        return None
        
    module = importlib.import_module(module_name)
    return getattr(module, class_name)


def iterable(obj):
    """Check if an object is iterable.
    """
    try:
        iter(obj)
    except Exception:
        return False
    else:
        return True