# for comparing prediction of close and open vocab cases.

from pathlib import Path
import tensorflow as tf
from tensorflow import keras as K
import cosy
import sys
import csv




def finalize(vertices, mask, v_idxs, mask_values):
    """Create the labels and mask padding as well as OOV symbols.

    Args:
        vertices (Tensor): The sequences of vertices of the ASTs in DFS order.
            shape = (batch_size, seq_len)
        mask (Tensor): The sliding window mask. shape = (batch_size, seq_len)
        mask_values (list): The symbols to be masked as well.

    Returns:
        (tuple): A triple containing the tensors (vertices, labels, sample_weights)
    """
    # Split the vertices into the pair (vertices[..., :-1], vertices[..., 1:])
    vertices, labels = cosy.preprocessing.add_shifted_labels(vertices)

    # Assign a sample weight of zero to padding and OOV symbols.
    sample_weights = cosy.preprocessing.mask_sample(
        y_true=labels,
        sample_weight=mask[..., 1:],
        mask_values=mask_values,
        mask_dtype="int32"
    )

    vidxs_info = v_idxs[..., 1:]

    return vertices, labels, sample_weights, vidxs_info




def get_end_positions(tensor, end_code, start_idx):
    # NOTE: re-consider when batch_size != 1 
    end_positions = tf.where(tf.equal(tensor, end_code))

    # only consider the token from start_idx
    gte_mask = tf.math.greater_equal(end_positions, start_idx)
    lt_mask = tf.math.logical_not(gte_mask)

    result = tf.boolean_mask(end_positions, gte_mask)
    lt_pos = tf.boolean_mask(end_positions, lt_mask)

    if tf.math.not_equal(tf.size(lt_pos), 0):
        # get the idx right before the start idx
        # for considering the "open" token at the end of the previous window
        prev_token = lt_pos[-1:]
        result = tf.concat([prev_token, result], axis=-1)

    return result




def create_dataset(COMPRESSION, NUM_THREADS, VOCAB_SIZE, eval_file):

    # Load the data and create the final labels and sample_weights.
    config = {"compression": COMPRESSION, "num_threads": NUM_THREADS}
    reader = cosy.data.SequenceReader(["int32", "int32", "int32"], 3, shuffle_buffer_size=None) # non shuffle for using sharding later

    # create the evaluation dataset
    eval_dataset = reader(eval_file, **config)
    eval_dataset = eval_dataset.map(lambda x, mask, vidx: finalize(x, mask, vidx, [0, VOCAB_SIZE-1]))

    idx = int(sys.argv[1])
    dataset = eval_dataset.shard(num_shards=10, index=idx)

    return dataset




def get_symbols_with_vidx(eval_ds_openvocab, encoder_openvocab, csv_file):

    for i, v_l_s_i in enumerate(eval_ds_openvocab):
        _, labels, sample_weights, v_index = v_l_s_i
        # option 1: just get not-considered end token positions of the window
        start_idx = tf.math.argmax(sample_weights, axis=-1) # NOTE: is it true when batch_size != 1?
        end_token_positions = get_end_positions(labels, encoder_openvocab.end_code, start_idx)

        for t1, t2 in zip(end_token_positions[:-1], end_token_positions[1:]):
            t_start = tf.squeeze(t1 + 1)
            t_stop = tf.squeeze(t2 + 1)
            target_sequence = labels[t_start:t_stop]
            vidx = tf.squeeze(tf.math.reduce_mean(v_index[t_start:t_stop])).numpy()

            with open(csv_file, "a", newline="") as f:
                writer = csv.writer(f, delimiter=",")
                line = []
                line.append([encoder_openvocab.decode(i) for i in target_sequence])
                line.append(vidx)
                writer.writerow(line)

        if i % 500 == 0 :
            print("Finished {}".format(i))




if __name__ == "__main__":
    CSV_FOLDER = Path("../specialized-evaluations")
    idx = int(sys.argv[1])
    csv_file = CSV_FOLDER / "py_prediction_epoch10_ws2k_nospacetabnewline-idx{}-with-vidx.csv".format(idx)

    VERBOSITY_LEVEL = 1 # 0 = silent, 1 = progress bar, 2 = one line per epoch
    VOCAB_SIZE = 10000
    COMPRESSION = "GZIP"
    NUM_THREADS = 3
    MAX_RANK = 10
    MAX_LENGTH = 30 # also modify here!!!!!!
    DATA_DIR = Path("../data")

    eval_file_openvocab = str(DATA_DIR / "ml50-30-vidx" / "10000_python50k_HuggingFaceBPE_eval_ws2k_ml30_shiftmask_wempty_longoov_encoderml50_nowspace-tab-newline-vidx.tfrecord")
    encoding_file_openvocab = str(DATA_DIR / "ml50-30" / "10000_python100k_HuggingFaceBPE_encoder_ml50.json")

    encoder_openvocab = cosy.preprocessing.SubTokenEncoder.load(encoding_file_openvocab)

    # create the evaluation dataset
    eval_ds_openvocab = create_dataset(COMPRESSION, NUM_THREADS, VOCAB_SIZE, eval_file_openvocab)

    print("Getting symbols for each window in the comparison...")
    get_symbols_with_vidx(eval_ds_openvocab, encoder_openvocab, csv_file)
    print("Done!")