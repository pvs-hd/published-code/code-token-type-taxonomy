# for comparing prediction of close and open vocab cases.

from pathlib import Path
import tensorflow as tf
from tensorflow import keras as K
import cosy
import sys
import csv




class LRSchedule(K.optimizers.schedules.LearningRateSchedule):
    """Learning rate used to when training the model.

    We use the seam learning rate as in the original Transformer paper
    `Attention Is All You Need <https://arxiv.org/abs/1706.03762>`_.
    """
    def __init__(self, dim, warmup_steps=4000):
        super().__init__()

        self.dim = tf.cast(dim, tf.float32)
        self.warmup_steps = warmup_steps


    def __call__(self, step):
        arg1 = tf.math.rsqrt(step)
        arg2 = step * (self.warmup_steps ** -1.5)

        return tf.math.rsqrt(self.dim) * tf.math.minimum(arg1, arg2)




def finalize(vertices, mask, mask_values):
    """Create the labels and mask padding as well as OOV symbols.

    Args:
        vertices (Tensor): The sequences of vertices of the ASTs in DFS order.
            shape = (batch_size, seq_len)
        mask (Tensor): The sliding window mask. shape = (batch_size, seq_len)
        mask_values (list): The symbols to be masked as well.

    Returns:
        (tuple): A triple containing the tensors (vertices, labels, sample_weights)
    """
    # Split the vertices into the pair (vertices[..., :-1], vertices[..., 1:])
    vertices, labels = cosy.preprocessing.add_shifted_labels(vertices)

    # Assign a sample weight of zero to padding and OOV symbols.
    sample_weights = cosy.preprocessing.mask_sample(
        y_true=labels,
        sample_weight=mask[..., 1:],
        mask_values=mask_values,
        mask_dtype="int32"
    )

    return vertices, labels, sample_weights




def get_end_positions(tensor, end_code, start_idx):
    # NOTE: re-consider when batch_size != 1 
    end_positions = tf.where(tf.equal(tensor, end_code))

    # only consider the token from start_idx
    gte_mask = tf.math.greater_equal(end_positions, start_idx)
    lt_mask = tf.math.logical_not(gte_mask)

    result = tf.boolean_mask(end_positions, gte_mask)
    lt_pos = tf.boolean_mask(end_positions, lt_mask)

    if tf.math.not_equal(tf.size(lt_pos), 0):
        # get the idx right before the start idx
        # for considering the "open" token at the end of the previous window
        prev_token = lt_pos[-1:]
        result = tf.concat([prev_token, result], axis=-1)

    return result




def create_dataset(COMPRESSION, NUM_THREADS, VOCAB_SIZE, eval_file):

    # Load the data and create the final labels and sample_weights.
    config = {"compression": COMPRESSION, "num_threads": NUM_THREADS}
    reader = cosy.data.SequenceReader(["int32", "int32"], 2, shuffle_buffer_size=None) # non shuffle for using sharding later

    # create the evaluation dataset
    eval_dataset = reader(eval_file, **config)
    eval_dataset = eval_dataset.map(lambda x, mask: finalize(x, mask, [0, VOCAB_SIZE-1]))

    idx = int(sys.argv[1])
    dataset = eval_dataset.shard(num_shards=10, index=idx)

    return dataset




def load_model(VOCAB_SIZE, MAX_RANK, checkpoint_filepath):

    # Define the model. We use the same dropout rate as for the original Transformer
    model = cosy.models.SequenceTransformer(vocab_size=VOCAB_SIZE, dropout=0.1)

    # setup optimizer and metrics
    learning_rate = LRSchedule(300)
    optimizer = K.optimizers.Adam(learning_rate, beta_1=0.9, beta_2=0.98, epsilon=1e-9)
    mrr = cosy.training.SparseMeanReciprocalRank(max_rank=MAX_RANK, name="MRR")
    topk = cosy.training.SparseTopK(k=5, name="TopK")

    model.compile(
        optimizer=optimizer,
        loss="sparse_categorical_crossentropy",
        weighted_metrics=["accuracy", mrr, topk]
    )

    # load model from the checkpoint
    model.load_weights(checkpoint_filepath).expect_partial()
    print("Trained model from {} was loaded.".format(checkpoint_filepath))

    return model




def convert_to_symbol_code(subtokens_sequence, subtoken_encoder, symbol_encoder):
    symbols = []
    subtokens = []
    for code in subtokens_sequence:
        if code != subtoken_encoder.end_code:
            subtokens.append(subtoken_encoder.decode(code))
        else:
            symbols.append("".join(subtokens))
            subtokens = []
    # encode symbols
    encoded_symbols = [symbol_encoder.encode(i) for i in symbols]
    return tf.convert_to_tensor(encoded_symbols)




def write_to_csv(csv_file, target_symbol, predicted_symbol, 
    target_sequence, predicted_sequence, predicted_symbol_open,
    encoder_closevocab, encoder_openvocab):
    write_to_file = []

    write_to_file.append(tf.squeeze(target_symbol).numpy()) # c_truth_code
    write_to_file.append(tf.squeeze(predicted_symbol).numpy()) # c_predict_code
    write_to_file.append(target_sequence.numpy().tolist()) # o_truth_code
    write_to_file.append(predicted_sequence.numpy().tolist()) # o_predict_code
    write_to_file.append(tf.reduce_all(tf.equal(predicted_symbol, target_symbol)).numpy()) # c_correct

    same_shape = tf.math.reduce_all(tf.math.equal(predicted_sequence.shape, target_sequence.shape))
    if same_shape:
        write_to_file.append(tf.reduce_all(tf.equal(predicted_sequence, target_sequence)).numpy()) # o_correct
    else:
        write_to_file.append("False") # o_correct
        
    write_to_file.append(tf.reduce_all(tf.equal(predicted_symbol, predicted_symbol_open)).numpy()) # same_prediction
    write_to_file.append(encoder_closevocab.decode(tf.squeeze(target_symbol))) # c_truth_symbol
    write_to_file.append(encoder_closevocab.decode(tf.squeeze(predicted_symbol))) # c_predict_symbol
    write_to_file.append([encoder_openvocab.decode(i) for i in target_sequence]) # o_truth_subtokens
    write_to_file.append([encoder_openvocab.decode(i) for i in predicted_sequence]) # o_predict_subtokens

    with open(csv_file, "a", newline="") as f:
        writer = csv.writer(f, delimiter=",")
        writer.writerow(write_to_file)




def compare_open_vs_close_vocab(model_openvocab, eval_ds_openvocab, encoder_openvocab, MAX_LENGTH,
        model_closevocab, encoder_closevocab, csv_file):
    
    token_count = 0
    token_total = 0
    symbol_count = 0
    symbol_total = 0

    for i, v_l_s in enumerate(eval_ds_openvocab):
        vertices, labels, sample_weights = v_l_s
        # option 1: just get not-considered end token positions of the window
        start_idx = tf.math.argmax(sample_weights, axis=-1) # NOTE: is it true when batch_size != 1?
        end_token_positions = get_end_positions(labels, encoder_openvocab.end_code, start_idx)

        for t1, t2 in zip(end_token_positions[:-1], end_token_positions[1:]):
            t_start = tf.squeeze(t1 + 1)
            t_stop = tf.squeeze(t2 + 1)

            input_sequence = vertices[:t_start+1]
            target_sequence = labels[t_start:t_stop] # label is shifted right 1 position
            sequence_weight = tf.math.reduce_mean(sample_weights[t_start:t_stop]) # return 0 type int32 if there is any 0 in tensor
            # mask = tf.cast(sample_weights[t_start:t_stop], dtype=tf.bool)

            try:
                predicted_sequence = cosy.algorithms.greedy_sequence_prediction(
                    lambda x: model_openvocab(x[tf.newaxis, ...]),
                    input_sequence,
                    encoder_openvocab.end_code,
                    MAX_LENGTH
                )

                # same_shape = predicted_sequence.shape.as_list() ==  target_sequence.shape.as_list()
                same_shape = tf.math.reduce_all(tf.math.equal(predicted_sequence.shape, target_sequence.shape))
                if same_shape:
                    if tf.reduce_all(tf.equal(predicted_sequence, target_sequence)):
                        token_count += sequence_weight

            # The model did not generate the end_token within the specified max_length
            except ValueError:
                pass

            input_symbols = convert_to_symbol_code(input_sequence, encoder_openvocab, encoder_closevocab)
            target_symbol = convert_to_symbol_code(target_sequence, encoder_openvocab, encoder_closevocab)
            predicted_symbol_open = convert_to_symbol_code(predicted_sequence, encoder_openvocab, encoder_closevocab)

            try:
                prediction = model_closevocab(input_symbols[tf.newaxis, ...])
                prediction = prediction[: ,-1:, :]  # (batch_size, 1, vocab_size)
                predicted_symbol = tf.squeeze(tf.cast(tf.argmax(prediction, axis=-1), dtype=tf.int32),axis=0)

                if tf.reduce_all(tf.equal(predicted_symbol, target_symbol)):
                    symbol_count += sequence_weight
            except ValueError:
                pass

            token_total += sequence_weight
            symbol_total += sequence_weight

            write_to_csv(csv_file, target_symbol, predicted_symbol, 
                target_sequence, predicted_sequence, predicted_symbol_open,
                encoder_closevocab, encoder_openvocab)

        if i % 500 == 0 :
            print("At {}th, token count: {}, token total: {}, symbol count: {}, symbol total: {}.".format(i, token_count, token_total, symbol_count, symbol_total))

    print("Size of dataset: {}".format(i))

    return token_count / token_total, symbol_count / symbol_total




if __name__ == "__main__":
    CSV_FOLDER = Path("../csvs")
    idx = int(sys.argv[1])
    csv_file = CSV_FOLDER / "jvs_open_vs_close_prediction_epoch10_ws2k_nospacetabnewline-idx{}.csv".format(idx)

    VERBOSITY_LEVEL = 1 # 0 = silent, 1 = progress bar, 2 = one line per epoch
    VOCAB_SIZE = 10000
    COMPRESSION = "GZIP"
    NUM_THREADS = 2
    MAX_RANK = 10
    MAX_LENGTH = 30 # also modify here!!!!!!
    DATA_DIR = Path("../data")
    CLOSE_DIR = Path("../data/closevocab")

    eval_file_openvocab = str(DATA_DIR / "ml50-30" / "10000_javascript50k_HuggingFaceBPE_eval_ws2k_ml30_shiftmask_wempty_longoov_encoderml50_nowspace-tab-newline.tfrecord")
    encoding_file_openvocab = str(DATA_DIR / "ml50-30" / "10000_javascript100k_HuggingFaceBPE_encoder_ml50.json")
    
    encoding_file_closevocab = str(CLOSE_DIR / "10000_javascript100k_StaticSymbolEncoder_encoder_nospace-tab-newline.json")

    WEIGHT_DIR = Path("../weights")
    WEIGHT_DIR_OPENVOCAB = WEIGHT_DIR / "JVS-WS2K-ML30-WITHEMPTY-LONGOOV-ENCODERML50-BATCH8-NOSPACETABNEWLINE"
    WEIGHT_DIR_CLOSEVOCAB = WEIGHT_DIR / "E0015-NOSPACETABNEWLINE"
    checkpoint_filepath_openvocab = str(WEIGHT_DIR_OPENVOCAB / "best_weights")
    checkpoint_filepath_closevocab = str(WEIGHT_DIR_CLOSEVOCAB / "best_weights")

    encoder_openvocab = cosy.preprocessing.SubTokenEncoder.load(encoding_file_openvocab)
    encoder_closevocab = cosy.preprocessing.StaticSymbolEncoder.load(encoding_file_closevocab)

    # create the evaluation dataset
    eval_ds_openvocab = create_dataset(COMPRESSION, NUM_THREADS, VOCAB_SIZE, eval_file_openvocab)

    # load model from the checkpoint
    model_openvocab = load_model(VOCAB_SIZE, MAX_RANK, checkpoint_filepath_openvocab)
    model_closevocab = load_model(VOCAB_SIZE, MAX_RANK, checkpoint_filepath_closevocab)

    print("Comparing prediction open vs close vocab case...")
    token_acc, symbol_acc = compare_open_vs_close_vocab(model_openvocab, eval_ds_openvocab, encoder_openvocab, MAX_LENGTH,
        model_closevocab, encoder_closevocab, csv_file)
    print("Token acc: {}, symbol acc: {}".format(token_acc, symbol_acc))