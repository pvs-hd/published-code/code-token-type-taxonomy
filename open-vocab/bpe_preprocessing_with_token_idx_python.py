import json
from pathlib import Path

import cosy
import scripts.utils as utils
import numpy as np
import matplotlib.pyplot as plt
import pickle
import numpy as np
import tensorflow as tf




def is_valid_symbol(symbol, max_length):
    """Check if a symbol is a valid symbol.
    I.e. symbol's length is less than or equal max_length.

    Args:
        symbol (string): The considered symbol.

    Returns:
        (bool)
    """
    if len(symbol) <= max_length:
        return True
    
    return False




def encode_with_cond(encoder, symbol, max_length, discard_long_symbols):
    """Encode a symbol using the given encoder.
    If the symbol's length is greater than max_length, an oov code is returned.
    """
    if discard_long_symbols:
        if is_valid_symbol(symbol, max_length):
            return encoder.encode(symbol)
        else:
            return [encoder.oov_code, encoder.end_code]
            
    return encoder.encode(symbol)




def left_shift(array_like, shift, fill_value):
    #  NOTE: be careful when the array_like has more than 1 dimension
    result = np.roll(array_like, -shift)
    result[-shift:] = fill_value
    return result




def run(in_file, out_file, encoder, window_size, step_size, compression, max_length, discard_long_symbols=True):
    """Run the preprocessing pipeline and create the tfrecord files.

    Args:
        in_file (Path): The name of the file that contains the raw datapoints.
        out_file (Path): The file where the tfrecords will be written to.
        encoder (StaticSymbolEncoder): The encoder.
        window_size (int): Size of the sliding window.
        step_size (int): Step size of the sliding window.
        compression (str): The type of compression that will be applied to the output file.
            Must be either 'GZIP', 'ZLIP' or None.
    """
    writer_config = {"filename": str(out_file), "compression": compression}

    # We map each AST in the Python150kDataset to a sequence of vertices is DFS order.
    with cosy.data.Python150kDataset(in_file, expand_values=True) as dataset,\
         cosy.data.SequenceWriter(["int32", "bool","int32"], 3, **writer_config) as writer:
        
        v_idx = 0

        for i, ast in enumerate(dataset):
            # Get all vertices in DFS order and encode their respective symbols.
            vertices = []
            v_idxs = []

            for v in cosy.tree.dfs(ast):
                # Leave the empty symbols as <ENDTOKEN>
                # replace all whitespaces in the symbol
                encoded_tokens = encode_with_cond(encoder, "".join(v["symbol"].split()), max_length, discard_long_symbols)

                if encoded_tokens:
                    vertices += encoded_tokens
                    v_idxs += [v_idx] * len(encoded_tokens)

                v_idx += 1

            # Create the final sequences with a sliding window. mask[i] is True if the symbol at
            # position i in the window is not already part of a previous window and False otherwise.
            # The window and mask may need to be cropped in case their boundaries lie within a
            # subtoken sequence.
            crop_beginning = False
            cropped_subtokens = 0

            for w_m_v, w_m_vidx in \
                zip(cosy.preprocessing.window_iter(vertices, window_size, step_size), 
                cosy.preprocessing.window_iter(v_idxs, window_size, step_size)):
                window, mask = w_m_v
                window_vidx, mask_vidx = w_m_vidx

                # left shift the mask according to the cropped subtokens of the previous window
                mask = left_shift(mask, cropped_subtokens, fill_value=1)

                if crop_beginning:
                    for position, symbol in enumerate(window):
                        if symbol == encoder.end_code:
                            window = window[position:]
                            mask = mask[position:]
                            window_vidx = window_vidx[position:]
                            break

                crop_beginning = True # always crop the begin token of next windows

                for position, symbol in enumerate(reversed(window)):
                    if symbol == encoder.end_code:
                        window = window[:len(window)-position]
                        mask = mask[:len(mask)-position]
                        window_vidx = window_vidx[:len(window_vidx)-position]
                        # tract the number of cropped elements to shift left the mask of the next window
                        cropped_subtokens = position
                        break

                window_vidx = window_vidx * mask

                writer(sequences=[window, mask, window_vidx])

        print("Finished creating the dataset for {}".format(in_file))




if __name__ == "__main__":
    VOCAB_SIZE = 10000
    WINDOW_SIZE = 2000
    STEP_SIZE = 1000
    COMPRESSION = "GZIP"
    FOLDER = Path("../data")
    END_TOKEN = "<ENDTOKEN>"

    eval_in_file = FOLDER / "python50k_eval.json"
    eval_out_file = FOLDER / "ml50-30-vidx" / "10000_python50k_HuggingFaceBPE_eval_ws2k_ml30_shiftmask_wempty_longoov_encoderml50_nowspace-tab-newline-vidx.tfrecord"
    # using encoder with max length
    encoding_file = FOLDER / "ml50-30" / "10000_python100k_HuggingFaceBPE_encoder_ml50.json"

    MAX_LENGTH_DATA = 30 # max length for creating dataset
    MAX_LENGTH_ENCODER = 50 # max length for building encoder
    OOV_TOKEN = "<UNKNOWN>"
    DISCARD_LONG_SYMBOLS = True

    # Create the root folder if necessary.
    FOLDER.mkdir(parents=True, exist_ok=True)

    encoder = cosy.preprocessing.SubTokenEncoder.load(encoding_file)

    # Create the evaluation data.
    print("Creating eval data...")
    run(eval_in_file, eval_out_file, encoder, WINDOW_SIZE, STEP_SIZE, COMPRESSION, MAX_LENGTH_DATA, DISCARD_LONG_SYMBOLS)

    print("Created/used files:\n{}\n{}".format(eval_out_file,encoding_file))
