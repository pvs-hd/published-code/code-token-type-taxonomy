# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import json
from pathlib import Path

import tokenizers
import cosy
import scripts.utils as utils
from collections import Counter
import statistics
import numpy as np
import matplotlib.pyplot as plt
import pickle
import numpy as np




def is_valid_symbol(symbol, max_length):
    """Check if a symbol is a valid symbol.
    I.e. symbol's length is less than or equal max_length.

    Args:
        symbol (string): The considered symbol.

    Returns:
        (bool)
    """
    if len(symbol) <= max_length:
        return True
    
    return False


def symbol_iterator(filepath, max_length, discard_long_symbols):
    """Load the dataset from the given file and iterate over every symbol in it.

    Args:
        filepath (string): The to the file that contains our dataset.
    Yields:
        (string): The symbols that occur in our dataset.
    """
    with cosy.data.Dataset(filepath, encoding="utf-8",convert_to_utf8=True) as dataset:
        for line in dataset:
            for node in json.loads(line):
                if utils.iterable(node):
                    if "value" in node:
                        if discard_long_symbols:
                            if is_valid_symbol(str(node["value"]), max_length):
                                yield str(node["value"])
                        else:
                            yield str(node["value"])

                    if "type" in node:
                        if discard_long_symbols:
                            if is_valid_symbol(str(node["type"]), max_length):
                                yield str(node["type"])
                        else:
                            yield str(node["type"])




def build_encoder(in_file, vocab_size, end_token, oov_token, max_length, discard_long_symbols=True):
    """Build the encoder that encodes our symbols as integers.

    Args:
        in_file (Path): The file that contains the raw datapoints.
        vocab_size (int): The size of our encoder vocabulary. We only encode the
            (vocab_size - 2) most frequent symbols together with the padding and end symbol.
        end_token (string): The symbols used to represent the end_token.

    Returns:
        (StaticSymbolEncoder) The encoder.
    """
    # CHECK vocab for the following:
    # 1) that all single characters contained in symbols are present in the vocab
    # 2) that all nonterminal symbols are present in the vocab (the "type" of nodes with children)
    # 3) compute length distribution of subtoken sequences.
    # The sub token encoder will always add the 3 special symbols <PADDING>, <UNKNOWN> and  <END>.
    vocab = cosy.preprocessing.build_bpe_vocab(symbol_iterator(in_file, max_length, discard_long_symbols), vocab_size - 3)
    return cosy.preprocessing.SubTokenEncoder(vocab, end_token=end_token, oov_token=oov_token)




def encode_with_cond(encoder, symbol, max_length, discard_long_symbols):
    """Encode a symbol using the given encoder.
    If the symbol's length is greater than max_length, an oov code is returned.
    """
    if discard_long_symbols:
        if is_valid_symbol(symbol, max_length):
            return encoder.encode(symbol)
        else:
            return [encoder.oov_code, encoder.end_code]
            
    return encoder.encode(symbol)




def left_shift(array_like, shift, fill_value):
    #  NOTE: be careful when the array_like has more than 1 dimension
    result = np.roll(array_like, -shift)
    result[-shift:] = fill_value
    return result




def run(in_file, out_file, encoder, window_size, step_size, compression, max_length, discard_long_symbols=True, convert_to_utf8=False):
    """Run the preprocessing pipeline and create the tfrecord files.

    Args:
        in_file (Path): The name of the file that contains the raw datapoints.
        out_file (Path): The file where the tfrecords will be written to.
        encoder (StaticSymbolEncoder): The encoder.
        window_size (int): Size of the sliding window.
        step_size (int): Step size of the sliding window.
        compression (str): The type of compression that will be applied to the output file.
            Must be either 'GZIP', 'ZLIP' or None.
    """
    writer_config = {"filename": str(out_file), "compression": compression}

    # We map each AST in the JavaScript150kDataset to a sequence of vertices is DFS order.
    with cosy.data.JavaScript150kDataset(in_file, encoding="utf-8",convert_to_utf8=convert_to_utf8, expand_values=True) as dataset,\
         cosy.data.SequenceWriter(["int32", "bool"], 2, **writer_config) as writer:

        cropped_symbols = 0
        invalid_symbols = 0
        total_symbols = 0
        symbol_length = Counter()
        token_length = Counter()

        for i, ast in enumerate(dataset):
            # Get all vertices in DFS order and encode their respective symbols.
            vertices = []

            for v in cosy.tree.dfs(ast):
                symbol_length.update([len(str(v["symbol"]))])

                if not is_valid_symbol(str(v["symbol"]), max_length):
                    invalid_symbols += 1

                # Leave the empty symbols as <ENDTOKEN>
                # replace all whitespaces in the symbol
                encoded_tokens = encode_with_cond(encoder, "".join(str(v["symbol"]).split()), max_length, discard_long_symbols)

                if encoded_tokens:
                    vertices += encoded_tokens
                    token_length.update([len(encoded_tokens)])

                # NOTE: there will be empty symbols in the total symbols!
                total_symbols += 1

            # Create the final sequences with a sliding window. mask[i] is True if the symbol at
            # position i in the window is not already part of a previous window and False otherwise.
            # The window and mask may need to be cropped in case their boundaries lie within a
            # subtoken sequence.
            crop_beginning = False
            cropped_subtokens = 0

            for window, mask in cosy.preprocessing.window_iter(vertices, window_size, step_size):
                # left shift the mask according to the cropped subtokens of the previous window
                mask = left_shift(mask, cropped_subtokens, fill_value=1)

                if crop_beginning:
                    for position, symbol in enumerate(window):
                        if symbol == encoder.end_code:
                            window = window[position:]
                            mask = mask[position:]
                            cropped_symbols += 1
                            break

                crop_beginning = True # always crop the begin token of next windows

                for position, symbol in enumerate(reversed(window)):
                    if symbol == encoder.end_code:
                        window = window[:len(window)-position]
                        mask = mask[:len(mask)-position]
                        # tract the number of cropped elements to shift left the mask of the next window
                        cropped_subtokens = position
                        break

                writer(sequences=[window, mask])

        print("Finished creating the dataset for {}. Total cropped symbols: {}, invalid symbols: {}, total symbols: {}, most common length: {}, max length: {}, min length: {}.\
            There are {} symbols haves min length."
            .format(in_file, cropped_symbols, invalid_symbols, total_symbols, symbol_length.most_common(1), max(symbol_length.keys()), min(symbol_length.keys()),
            symbol_length[min(symbol_length.keys())]))
        print("For the data set of {}, most common token length: {}, max token length: {}, min token length: {}, avg length: {}. There are {} symbols have min token length."
            .format(in_file, token_length.most_common(1), max(token_length.keys()), min(token_length.keys()), 
            statistics.mean(token_length.elements()), token_length[min(token_length.keys())]))

        return symbol_length, token_length




def save_counter(counter, filepath):
    with open(filepath, "wb") as outfile:
        pickle.dump(counter, outfile)




if __name__ == "__main__":
    VOCAB_SIZE = 10000
    WINDOW_SIZE = 2000
    STEP_SIZE = 1000
    COMPRESSION = "GZIP"
    FOLDER = Path("../data")
    END_TOKEN = "<ENDTOKEN>"

    train_in_file = FOLDER / "programs_training.json"
    eval_in_file = FOLDER / "programs_eval.json"
    train_out_file = FOLDER / "ml50-30" / "10000_javascript100k_HuggingFaceBPE_train_ws2k_ml30_shiftmask_wempty_longoov_encoderml50_nowspace-tab-newline.tfrecord"
    eval_out_file = FOLDER / "ml50-30" / "10000_javascript50k_HuggingFaceBPE_eval_ws2k_ml30_shiftmask_wempty_longoov_encoderml50_nowspace-tab-newline.tfrecord"
    # using encoder with max length
    encoding_file = FOLDER / "ml50-30" / "10000_javascript100k_HuggingFaceBPE_encoder_ml50.json"

    train_symbol_len_file = FOLDER / "ml50-30" / "10000_javascript100k_HuggingFaceBPE_symbol_ws2k_ml30_shiftmask_wempty_longoov_encoderml50_nowspace-tab-newline.pickle"
    train_token_len_file = FOLDER / "ml50-30" / "10000_javascript100k_HuggingFaceBPE_token_ws2k_ml30_shiftmask_wempty_longoov_encoderml50_nowspace-tab-newline.pickle"
    eval_symbol_len_file = FOLDER / "ml50-30" / "10000_javascript50k_HuggingFaceBPE_symbol_ws2k_ml30_shiftmask_wempty_longoov_encoderml50_nowspace-tab-newline.pickle"
    eval_token_len_file = FOLDER / "ml50-30" / "10000_javascript50k_HuggingFaceBPE_token_ws2k_ml30_shiftmask_wempty_longoov_encoderml50_nowspace-tab-newline.pickle"

    MAX_LENGTH_DATA = 30 # max length for creating dataset
    MAX_LENGTH_ENCODER = 50 # max length for building encoder
    OOV_TOKEN = "<UNKNOWN>"
    DISCARD_LONG_SYMBOLS = True

    # Create the root folder if necessary.
    FOLDER.mkdir(parents=True, exist_ok=True)

    # Download the data if it does not already exist.
    if not (train_in_file.is_file() and eval_in_file.is_file()):
        cosy.data.JavaScript150kDataset.download(path=FOLDER)

    # Create the encoding if it does not already exist.
    if not encoding_file.is_file():
        print("Building encoder...")
        encoder = build_encoder(train_in_file, VOCAB_SIZE, END_TOKEN, OOV_TOKEN, MAX_LENGTH_ENCODER, DISCARD_LONG_SYMBOLS)
        encoder.save(encoding_file)
    else:
        encoder = cosy.preprocessing.SubTokenEncoder.load(encoding_file)

    # Create the trainings data.
    print("Creating train data...")
    train_symbol_len, train_token_len = run(train_in_file, train_out_file, encoder, WINDOW_SIZE, STEP_SIZE, COMPRESSION, MAX_LENGTH_DATA, DISCARD_LONG_SYMBOLS, convert_to_utf8=False)
    save_counter(train_symbol_len, str(train_symbol_len_file))
    save_counter(train_token_len, str(train_token_len_file))

    # Create the evaluation data.
    print("Creating eval data...")
    eval_symbol_len, eval_token_len = run(eval_in_file, eval_out_file, encoder, WINDOW_SIZE, STEP_SIZE, COMPRESSION, MAX_LENGTH_DATA, DISCARD_LONG_SYMBOLS, convert_to_utf8=True)
    save_counter(eval_symbol_len, str(eval_symbol_len_file))
    save_counter(eval_token_len, str(eval_token_len_file))

    print("Created/used files:\n{}\n{}\n{}\n{}\n{}\n{}\n{}".format(train_out_file, eval_out_file,
        encoding_file, train_symbol_len_file, train_token_len_file, eval_symbol_len_file, eval_token_len_file))
